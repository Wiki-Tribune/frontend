import axios from "axios";
export default {
  namespaced: true,
  state: {
    Fetched: false,
    DatePrefix: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
    },
    Page: 1,
    ChangeList: [],
  },
  getters: {
    GET_CHANGE_LIST_FETCHED: (state) => {
      return state.Fetched;
    },
    GET_CHANGE_LIST_PAGE: (state) => {
      return state.Page;
    },
    GET_CHANGE_LIST: (state) => {
      return state.ChangeList;
    },
    GET_DATE_PREFIX: (state) => {
      return state.DatePrefix;
    },
  },
  mutations: {
    SET_CHANGE_FETCHED(state, payload) {
      state.Fetched = payload;
    },
    SET_CHANGE_LIST(state, payload) {
      state.ChangeList = payload;
    },
    SET_DATE_PREFIX_PART(state, payload) {
      if (payload.part == "month" && payload.value < 1) {
        state.DatePrefix["month"] = 12;
        state.DatePrefix["year"]--;
      } else if (payload.part == "month" && payload.value > 12) {
        state.DatePrefix["month"] = 1;
        state.DatePrefix["year"]++;
      } else {
        state.DatePrefix[payload.part] = payload.value;
      }
    },
  },
  actions: {
    CHANGE_DATE_PREFIX_PART({ commit, dispatch }, datePrefixPayload) {
      commit("SET_DATE_PREFIX_PART", datePrefixPayload);
      dispatch("FETCH_CHANGE_LIST");
    },
    FETCH_CHANGE_LIST({ state, commit, rootGetters }) {
      commit("SET_CHANGE_FETCHED", false);

      var url;
      // if (state.SubwikiFilter == '') {
      url =
        process.env.VUE_APP_APIBASEURL +
        "change/bymonth/" +
        state.DatePrefix.year +
        "-" +
        window.numpad(state.DatePrefix.month, 2);
      // } else {
      // url = process.env.VUE_APP_APIBASEURL + "change/bysubwiki/"+state.SubwikiFilter;
      // }
      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_CHANGE_FETCHED", true);
          if (typeof response.data.Items != "undefined") {
            commit("SET_CHANGE_LIST", response.data.Items);
          } else {
            commit("SET_CHANGE_LIST", {});
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
};
