import Vue from "vue";
import axios from "axios";
import router from "../../router/index";

export default {
  namespaced: true,
  state: {
    Fetched: [],
    isFetching: false,
    Page: 1,
    SubwikiFilter: "",
    UserProfileFilter: "",
    UserProfilePostTab: null,
    UserProfilePostTabRoute: null,
    BlankPost: {
      userprofile: {
        slug: "",
        name: "",
      },
      subwiki: {
        slug: "",
        label: "",
      },
      cardUrl: null,
      parent: {
        pk: null,
        sk: null,
      },
      postText: "",
      parentSK: "",
      collaborative: true,
    },
    NewPost: {
      postText: null,
      collaborative: true,
      userprofile: null,
      subwiki: {
        slug: null,
        label: null,
      },
      cardUrl: null,
      parent: {
        pk: null,
        sk: null,
      },
    },
    PostList: [],
    PostListMode: null,
    PostReading: {
      createdAt: null,
      slug: "",
      profileFilter: null,
      cardUrl: null,
      subwiki: null,
      userprofile: null,
      postText: "",
      collaborative: null,
    },
    Debug: [],
    UIFeedbackMsg: "",
    IgnoreLinks: [],
    LastEvaluatedKey: null,
    EndOfList: false,
    fetchHandler: null,
    indexOfArchiveItem: null
  },
  getters: {
    GET_POST_LIST_FETCHED: (state) => {
      return state.Fetched;
    },
    GET_POST_LIST_PAGE: (state) => {
      return state.Page;
    },
    GET_POST_DEBUG: (state) => {
      return state.Debug;
    },
    GET_POST_UI_FEEDBACK: (state) => {
      return state.UIFeedbackMsg;
    },
    GET_NEW_POST: (state) => {
      return state.NewPost;
    },
    GET_NEW_POST_SUBWIKI: (state) => {
      return state.NewPost.subwiki;
    },
    GET_NEW_POST_PARENT: (state) => {
      return state.NewPost?.parent;
    },
    GET_NEW_POST_CARD_URL: (state) => {
      return state.NewPost.cardUrl;
    },
    GET_POSTLIST_SUBWIKI: (state) => {
      return state.SubwikiFilter;
    },
    GET_NEW_POST_TEXT: (state) => {
      return state.NewPost.postText;
    },
    GET_NEW_POST_COLLABORATIVE: (state) => {
      return state.NewPost.collaborative;
    },
    GET_POST_LIST: (state) => {
      return state.PostList;
    },
    GET_POST_LIST_MODE: (state) => {
      return state.PostListMode;
    },
    GET_LIST_INPUT: (state) => {
      return state.ListInput;
    },
    GET_POST_READING: (state) => {
      return state.PostReading;
    },
    GET_IGNORE_LINKS: (state) => {
      return state.IgnoreLinks;
    },
    GET_END_OF_POST_LIST: (state) => {
      return state.EndOfList;
    },
    GET_USERPROFILE_POSTTAB: (state) => {
      return state.UserProfilePostTab;
    },
    GET_USERPROFILE_POSTTABROUTE: (state) => {
      return state.UserProfilePostTabRoute;
    },
    GET_LAST_EVALUATED_KEY_POSTS: (state) => {
      return state.LastEvaluatedKey;
    },
  },
  mutations: {
    ADD_POST_FETCHED(state, payload) {
      if (state.Fetched.indexOf(payload < 0)) {
        state.Fetched.push(payload);
      }
    },
    RESET_POST_LIST_FETCHED(state) {
      state.Fetched = [];
    },
    RESET_END_OF_LIST(state) {
      state.EndOfList = false;
    },
    SET_END_OF_LIST(state) {
      state.EndOfList = true;
    },
    ADD_DEBUG(state, payload) {
      state.Debug.push(payload);
    },
    SET_UI_FEEDBACK(state, payload) {
      state.UIFeedbackMsg = payload;
    },
    SET_POST_READING(state, payload) {
      state.PostReading = payload;
    },
    SET_POST_READING_TEXT(state, payload) {
      Vue.set(state.PostReading, "postText", payload);
    },
    SET_POST_READING_STAT(state, payload) {
      let newValue;
      let baseValue = 0;

      if (state.PostReading.statistics[payload.statistic]) {
        baseValue = state.PostReading.statistics[payload.statistic];
      }

      if (payload.action == "increment") {
        newValue = baseValue + 1;
      } else if (payload.action == "decrement") {
        newValue = baseValue - 1;
      }
      Vue.set(state.PostReading.statistics, payload.statistic, newValue);
    },
    SET_LIST(state, payload) {
      state.PostList = payload;
    },
    SET_LIST_MODE(state, payload) {
      state.PostListMode = payload;
    },
    PUSH_TO_LIST(state, payload) {
      let i;
      for (i in payload) {
        state.PostList.push(payload[i]);
      }
    },
    SET_POST(state, payload) {
      Vue.set(state.PostList[payload.index], "postText", payload.newText);
      Vue.set(state.PostList[payload.index], "cardUrl", payload.cardUrl);
    },
    DELETE_POST(state, payload) {
      Vue.delete(state.PostList, payload.index);
    },
    SET_POST_STAT(state, payload) {
      let newValue;
      let baseValue = 0;

      if (state.PostList[payload.index]["statistics"][payload.statistic]) {
        baseValue =
          state.PostList[payload.index]["statistics"][payload.statistic];
      }

      if (payload.action == "increment") {
        newValue = baseValue + 1;
      } else if (payload.action == "decrement") {
        newValue = baseValue - 1;
      }

      Vue.set(
        state.PostList[payload.index]["statistics"],
        payload.statistic,
        newValue
      );
    },
    PREPEND_LIST(state, payload) {
      state.PostList.unshift(payload);
    },
    SET_LIST_INPUT(state, payload) {
      state.ListInput = payload;
    },
    SET_NEW_POST(state, payload) {
      state.NewPost = payload;
    },
    SET_SUBWIKI_FILTER(state, payload) {
      state.SubwikiFilter = payload;
    },
    SET_USERPROFILE_FILTER(state, payload) {
      state.UserProfileFilter = payload;
    },
    SET_USERPROFILE_POSTTAB(state, payload) {
      state.UserProfilePostTab = payload;
    },
    SET_USERPROFILE_POSTTABROUTE(state, payload) {
      state.UserProfilePostTabRoute = payload;
    },
    SET_NEW_POST_SUBWIKI(state, payload) {
      /*
        OMG - so we've into the sitation where we use both subwikiLabel and 
        label in different places and trying to only set one of them causes the 
        autocomplete vuetify component to break.
        Until we have the time to unify these two variables I'm leaving both in there
        because that makes it all work.
        = Simon.
      */
      Vue.set(state.NewPost, "subwiki", {
        label: payload.subwikiLabel,
        subwikiLabel: payload.subwikiLabel,
        slug: payload.slug,
      });
    },
    SET_NEW_POST_PARENT(state, payload) {
      if (payload.type == "subwiki") {
        Vue.set(state.NewPost, "parent", {
          pk: "subwiki#" + payload.slug,
          sk: "subwiki#" + payload.slug,
        });
      } else if (payload.type == "userprofile") {
        Vue.set(state.NewPost, "parent", {
          pk: "userprofile#" + payload.slug,
          sk: "userprofile#" + payload.slug,
        });
      } else {
        Vue.set(state.NewPost, "parent", {
          pk: "maintrunk#maintrunk",
          sk: "maintrunk#maintrunk",
        });
      }
    },
    SET_NEW_POST_USERPROFILE(state, payload) {
      state.NewPost.userprofile = payload;
    },
    SET_NEW_POST_TEXT(state, payload) {
      state.NewPost.postText = payload;
    },
    SET_NEW_POST_COLLABORATIVE(state, payload) {
      state.NewPost.collaborative = payload;
    },
    SET_NEW_POST_CARD_URL(state, payload) {
      state.NewPost.cardUrl = payload;
    },
    ADD_IGNORE_LINK(state, payload) {
      state.IgnoreLinks.push(payload);
    },
    RESET_IGNORE_LINKS(state) {
      state.IgnoreLinks = [];
    },
    INCREMENT_POST_COMMENT_COUNT(state, payload) {
      let i;
      for (i in state.PostList) {
        if (
          state.postList[i].pk == payload.pk &&
          state.postList[i].sk == payload.sk
        ) {
          Vue.set(
            state.postList[i].statistics,
            "commentCount",
            state.postList[i].statistics.commentCount + 1
          );
          break;
        }
      }
    },
    SET_LAST_EVALUATED_KEY_POSTS(state, payload) {
      state.LastEvaluatedKey = payload;
    },
    SET_IS_FETCHING(state, payload) {
      state.isFetching = payload;
    },
  },
  actions: {
    UPDATE_POST_ITEM({ state, commit }, payload) {
      let i = 0;
      let updateIndex;
      for (i in state.PostList) {
        if (state.PostList[i].slug == payload.slug) {
          updateIndex = i;
          break;
        }
      }
      if (updateIndex) {
        commit("SET_POST", {
          index: updateIndex,
          newText: payload.newText,
          cardUrl: payload.cardUrl,
        });
      }
      // commit("SET_LIST", newList);
    },
    DELETE_POST_ITEM({ state, commit }, payload) {
      let i = 0;
      let updateIndex;
      for (i in state.PostList) {
        if (state.PostList[i].slug == payload.slug) {
          updateIndex = i;
          break;
        }
      }
      if (updateIndex) {
        state.indexOfArchiveItem = updateIndex
        commit("DELETE_POST", {
          index: updateIndex,
        });
      }
    },
    RESTORE_POST_ITEM({state}, payload){
      state.PostList.splice(state.indexOfArchiveItem, 0, payload)
    },
    UPDATE_POST_STAT({ state, commit }, payload) {
      let i = 0;
      let updateIndex;
      for (i in state.PostList) {
        if (state.PostList[i].slug == payload.slug) {
          updateIndex = i;
          break;
        }
      }

      if (typeof updateIndex != "undefined") {
        let update = {
          index: updateIndex,
          statistic: payload.statistic,
          action: payload.action,
        };
        commit("SET_POST_STAT", update);

        // WHY do we duplicate the post that the user is reading?

        if (state.PostReading.slug == payload.slug) {
          commit("SET_POST_READING_STAT", update);
        }
      }
      // commit("SET_LIST", newList);
    },

    // UPDATE_NEW_POST({ commit }, payload) {
    //   commit("SET_NEW_POST", payload);
    // },
    UPDATE_POSTLIST_SUBWIKI({ commit }, payload) {
      commit("SET_SUBWIKI_FILTER", payload);
    },
    UPDATE_POSTLIST_USERPROFILE({ commit }, payload) {
      commit("SET_USERPROFILE_FILTER", payload);
    },
    UPDATE_POSTLIST_USERPROFILE_POSTTAB({ commit }, payload) {
      commit("SET_USERPROFILE_POSTTAB", payload);
    },
    UPDATE_POSTLIST_USERPROFILE_POSTTABROUTE({ commit }, payload) {
      commit("SET_USERPROFILE_POSTTABROUTE", payload);
    },
    UPDATE_POSTLIST_MODE({ commit }, payload) {
      commit("SET_LIST_MODE", payload);
    },
    RESET_POST_LIST_FETCHED({ commit }) {
      commit("RESET_POST_LIST_FETCHED");
      commit("RESET_END_OF_LIST");
    },

    UPDATE_NEW_POST_PARENT({ commit }, payload) {
      if (payload.mode) {
        commit("SET_NEW_POST_PARENT", {
          type: payload.mode,
          slug: payload.parent.slug,
        });
        if (payload.mode == "subwiki") {
          commit("SET_NEW_POST_SUBWIKI", payload.parent);
          commit("SET_NEW_POST_USERPROFILE", {});
        } else if (payload.mode == "userprofile") {
          commit("SET_NEW_POST_SUBWIKI", {});
          commit("SET_NEW_POST_USERPROFILE", {
            fname: payload.parent.fname,
            lname: payload.parent.lname,
            slug: payload.parent.slug,
            userID: payload.parent.userID,
          });
        }
      }
    },
    UPDATE_NEW_POST_SUBWIKI({ commit }, payload) {
      if (payload === null) {
        commit("SET_NEW_POST_SUBWIKI", {});
        commit("SET_NEW_POST_PARENT", {
          type: "subwiki",
          slug: "",
        });
      } else if (payload.slug != "-none-") {
        commit("SET_NEW_POST_SUBWIKI", payload);
        if (payload) {
          commit("SET_NEW_POST_PARENT", {
            type: "subwiki",
            slug: payload.slug,
          });
        }
      }
    },
    // UPDATE_NEW_POST_USER_PROFILE({ commit }, payload) {
    //   commit("SET_NEW_POST_USERPROFILE", payload);
    //   if (payload) {
    //     commit("SET_NEW_POST_PARENT", {
    //       type: "userprofile",
    //       slug: payload.slug,
    //     });
    //   }
    // },
    UPDATE_LIST_INPUT({ commit }, payload) {
      commit("SET_LIST_INPUT", payload);
    },
    INCREMENT_POST_COMMENT_COUNT({ commit }, payload) {
      commit("INCREMENT_POST_COMMENT_COUNT", payload);
    },
    UPDATE_NEW_POST_TEXT({ commit }, payload) {
      commit("SET_NEW_POST_TEXT", payload);
    },
    UPDATE_NEW_POST_COLLABORATIVE({ commit }, payload) {
      commit("SET_NEW_POST_COLLABORATIVE", payload);
    },
    UPDATE_NEW_POST_CARD_URL({ commit }, payload) {
      commit("SET_NEW_POST_CARD_URL", payload);
    },
    DETACH_LINK_CARD({ commit, state }) {
      commit("ADD_IGNORE_LINK", state.NewPost.cardUrl);
      commit("SET_NEW_POST_CARD_URL", "");
    },
    RESET_POST({ commit }) {
      commit("SET_NEW_POST_TEXT", "");
      commit("SET_NEW_POST_CARD_URL", "");
    },
    RESET_POST_LIST({ commit }) {
      commit("SET_LIST", {});
      commit("RESET_POST_LIST_FETCHED", {});
    },

    CREATE_NEW_POST({ commit, state, dispatch, rootGetters }) {
      var url = process.env.VUE_APP_APIBASEURL + "post";
      if (!state.NewPost.parent || !state.NewPost.parent.pk) {
        return false;
      }
      axios
        .post(url, state.NewPost, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if (typeof response.data.sk != "undefined") {
            dispatch("RESET_POST");
            commit("SET_UI_FEEDBACK", "Posted!");
            setTimeout(() => {
              commit("SET_UI_FEEDBACK", "");
            }, 800);
            commit("RESET_IGNORE_LINKS");
            commit("SET_NEW_POST_CARD_URL", "");
            if (
              state.PostListMode == "subwiki" ||
              state.PostListMode == "everything"
            ) {
              commit("PREPEND_LIST", response.data);
              dispatch("SET_LAST_EVALUATED_KEY_POSTS", null);
              dispatch("FETCH_POST_LIST");
            } else {
              commit("InboxStore/PREPEND_FEED_LIST", response.data, {
                root: true,
              });
              dispatch("InboxStore/SET_LAST_EVALUATED_KEY", null, {
                root: true,
              });
              dispatch("InboxStore/FETCH_FEED", { root: true });
            }
          }
          commit("ADD_DEBUG", response);
        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
    },
    //     PICK_POST_LIST({ commit, state }) {
    //       var url;
    // //////////////////////////////////////////////////////
    //       url = process.env.VUE_APP_APIBASEURL + "post/pick";
    //       axios
    //         .post(
    //           url,
    //           { KeyList: state.ListInput.split(/\n/) },
    //           {
    //             headers: { "content-type": "application/json" },
    //           }
    //         )
    //         .then(function (response) {
    //           commit("ADD_POST_FETCHED", state.Page);
    //           if (typeof response.data.Items != "undefined") {
    //             commit("SET_LIST", response.data.Items);
    //           } else {
    //             commit("SET_LIST", {});
    //             commit("ADD_DEBUG", "Empty post list");
    //           }
    //         })
    //         .catch(function (error) {
    //           commit("ADD_DEBUG", error);
    //         });
    //     },
    CLEAR_POST_LIST({ commit }) {
      commit("SET_LIST", {});
    },
    FETCH_POST_LIST({ commit, state, rootGetters }, listRoutePayload) {
      if (state.IsFetching) {
        return false;
      }
      commit('SET_IS_FETCHING', true);

      if (state.EndOfList) {
        return true;
      }
      var url;
      if (state.SubwikiFilter != "") {
        url =
          
          "post/ref-subwiki/" +
          state.SubwikiFilter;
      } else if (state.UserProfileFilter != "") {
        let route;
        if (
          ["posts", "contributions", "branch"].includes(
            state.UserProfilePostTab
          )
        ) {
          route = state.UserProfilePostTab;
        } else {
          console.log("Not sure what route to use!");
          return false;
        }
        url = `post/ref-userprofile/${route}/${state.UserProfileFilter}`;
      } else {
        url =  "post";
      }
      if (state.LastEvaluatedKey != null) {
        let lastevaluatedkeyurl;
        if (listRoutePayload == "contributions") {
          lastevaluatedkeyurl =
            state.LastEvaluatedKey.createdAt +
            "_" +
            state.LastEvaluatedKey.sk +
            "_" +
            state.LastEvaluatedKey.pk;
        } else {
          lastevaluatedkeyurl =
            state.LastEvaluatedKey.sk + "_" + state.LastEvaluatedKey.pk;
        }
        url += "?start=" + encodeURIComponent(lastevaluatedkeyurl);
      }
      const instance = axios.create({
        baseURL: process.env.VUE_APP_APIBASEURL,
        timeout: 10000,
    });
      state.fetchHandler = instance
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit('SET_IS_FETCHING', false);

          commit("ADD_POST_FETCHED", state.Page);
          if (typeof response.data.Items != "undefined") {
            let list = response.data.Items;
            let i;
            try {
              for (i in list) {
                if (!list[i].statistics) {
                  list[i].statistics = {};
                }
                if (!list[i].statistics?.commentCount) {
                  list[i].statistics.commentCount = 0;
                }
              }
            } catch (e) {
              // console.log(e);
            }
            if (state.LastEvaluatedKey != null) {
              commit("PUSH_TO_LIST", list);
            } else {
              commit("SET_LIST", list);
            }
            if (response.data.LastEvaluatedKey) {
              commit(
                "SET_LAST_EVALUATED_KEY_POSTS",
                response.data.LastEvaluatedKey
              );
            } else {
              commit("SET_END_OF_LIST");
            }
          } else {
            if (state.LastEvaluatedKey == null) {
              commit("SET_LIST", {});
            } else {
              commit("SET_END_OF_LIST");
            }
            commit("ADD_DEBUG", "Empty post list");
          }
        })
        .catch(function (error) {
          commit("ADD_DEBUG", error);
        });
    },
    UPDATE_POST_READING_TEXT({ state, commit }, payload) {
      if (state.PostReading.slug) {
        commit("SET_POST_READING_TEXT", payload);
      }
    },
    FETCH_POST_READING({ commit, rootGetters, dispatch }, payload) {
      let url = process.env.VUE_APP_APIBASEURL + "post/id/" + payload;
      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if (
            Object.keys(response.data).length != 0 &&
            typeof response.data.postText != "undefined"
          ) {
            commit("SET_POST_READING", response.data);
            if (!response.data.blocked) {
              commit("ADD_DEBUG", "Successfuly fetched post");
              if (response.data.statistics.commentCountTopLevel === 0) {
                dispatch(
                  "InterfaceStore/SET_SHOW_COMMENTCREATE_ARRAY",
                  response.data.sk,
                  { root: true }
                );
              }
            }
          } else {
            router.push({
              name: "404 - Page not 'here'",
              params: { LANG: payload },
            });
            commit("SET_POST_READING", {});
            commit("ADD_DEBUG", "Empty single post");
          }
        })
        .catch(function (error) {
          if (error.response.status === 404) {
            console.log(error.response.status);
            console.log("error.response.status");
            router.push({
              name: "404 - Page not 'here'",
              params: { LANG: payload },
            });
          }
          commit("ADD_DEBUG", error);
        });
    },
    ARCHIVE_POST({commit, rootGetters, dispatch }, payload) {
      dispatch("DELETE_POST_ITEM", payload)
      var url = "";
      if (payload.userslug == rootGetters["UserStore/userSlug"]) {
        url = process.env.VUE_APP_APIBASEURL + "post/archivepostself";
      } else {
        url = process.env.VUE_APP_APIBASEURL + "post/archive";
      }

      axios
        .put(url, payload, rootGetters["UserStore/authHeader"])
        .then(function () {
          console.log("Item deleted")
        })
        .catch(function (error) {
          dispatch("RESTORE_POST_ITEM", payload)
          commit("ADD_DEBUG", error);
        });
    },
    RESET_POST_READING({ commit }) {
      commit("SET_POST_READING", {});
    },
    SET_LAST_EVALUATED_KEY_POSTS({ commit }, payload) {
      commit("SET_LAST_EVALUATED_KEY_POSTS", payload);
    },
  },
};
