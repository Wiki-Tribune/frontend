import axios from "axios"

export default {
  namespaced: true,
  state: { 
    archivingposts: false,
    postsArchived: false,
    debug: []
  },
  actions: {
    // ALERT_SPAM(payload){
    //     console.log("WE ARE ALERTING SPAM")  
    // },
    ARCHIVE_USER({commit, state, rootGetters}, payload){
      commit("CLEAR_DEBUG")
      state.archivingposts = true
      var url = process.env.VUE_APP_MODERATIONBASEURL + "archiveuser"
      var data = {
        "userslug": payload
      };
      axios
      .put(url, data, rootGetters["UserStore/authHeader"])
      .then(function () {
        state.archivingposts = false
        commit("POSTS_ARCHIVED")
      })
      .catch(function (error) {
        state.archivingposts = false
        console.log(error)  
        commit('ADD_DEBUG', "Could not archive posts")
      })
    },
    ARCHIVE_POST({commit, dispatch, rootGetters}, payload){
      let putdata = {
        key: {
          pk: payload.key.pk,
          sk: payload.key.sk,
        },
        userslug: payload.createdByUserSlug,
        slug: payload.slug,
      }
      if(rootGetters["PostStore/GET_POST_LIST"]){
        dispatch("PostStore/DELETE_POST_ITEM", putdata, { root: true })
      }
      if(rootGetters["InboxStore/GET_FEED"]){
        console.log("Hell yeah we have items in inboxStore")
        dispatch("InboxStore/DELETE_POST_ITEM", putdata, { root: true })
      }
      var url = "";
      if (payload.userslug == rootGetters["UserStore/userSlug"]) {
        url = process.env.VUE_APP_APIBASEURL + "post/archivepostself";
      } else {
        url = process.env.VUE_APP_APIBASEURL + "post/archive";
      }
      axios
        .put(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          if(rootGetters["PostStore/GET_POST_LIST"]){
          dispatch("PostStore/RESTORE_POST_ITEM", putdata, { root: true })
          commit("PostStore/ADD_DEBUG", error, { root: true });}
          if(rootGetters["InboxStore/GET_FEED"]){
            dispatch("InboxStore/RESTORE_POST_ITEM", putdata, { root: true })
          commit("InboxStore/ADD_DEBUG", error, { root: true });}
            
          });
    },
    ARCHIVE({dispatch}, payload){
      if(payload.type === "post"){
        dispatch("ARCHIVE_POST", payload)
      }
      else if (payload.type === "comment"){
        let putdata = {
          key: {
            pk: payload.key.pk,
            sk: payload.key.sk,
          },
          userslug: payload.userslug,
        }
        console.log("/////////////////////////////")
        console.log(putdata)
        console.log("We're archiving a comment now")
        console.log("/////////////////////////////")
        dispatch("CommentStore/ARCHIVE_COMMENT", putdata, {root:true})
      }
      else{
        console.log("Missing deletion item/data")
      }
    }
  },
  mutations: {
    CLEAR_DEBUG(state){
      state.debug = []
    },
    POSTS_ARCHIVED(state){
      state.postsArchived = true
    }
  },
  getters: {
    GET_POST_ARCHIVE_DEBUG(state){
      return state.usergroups
    },
    GET_POST_ARCHIVE_STATUS(state){
      return state.postsArchived
    },
    GET_POST_ARCHIVING_STATUS(state){
      return state.archivingposts
    }
  }
}