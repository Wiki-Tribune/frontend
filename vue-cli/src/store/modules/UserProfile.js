import Vue from 'vue'
import axios from "axios";
import router from "../../router/index";
export default {
  namespaced: true,
  state: {
    MyProfileLastEdit: {
      time: null,
      what: null
    },
    IsEditingMyProfile: false,
    Debug: ["Test"],
    UIFeedbackMsg: "",
    UserProfiles: {},
    LastSlugLoaded: null,
    HasLoaded: [],
    ProfileChanges: {},
    DefaultUserProfile: {
      userSlug: null,
      fname: null,
      lname: null,
    },
  },
  getters: {
    IS_MY_PROFILE: (state, getters, rootState) => (slug) => {
      // This should be regardless of if we already have fetched the full information
      if (        
        rootState.UserStore?.userprofile?.slug == slug
      ) {
        return true;
      } else {
        return false;
      }
    },
    URL_PROFILE_PIC: (state, getters, rootState) => (userID) => {
      
      let url = process.env.VUE_APP_S3URL + 'profilepics/' + userID;
      if (userID == rootState.UserStore?.userprofile?.userID && state.MyProfileLastEdit.time) {
        url += '?'+state.MyProfileLastEdit.time.toISOString()
      }
      return url;
    },
    URL_COVER_PIC: (state, getters, rootState) => (userID) => {
      let url = process.env.VUE_APP_S3URL + 'coverpics/' + userID;
      if (userID == rootState.UserStore?.userprofile?.userID && state.MyProfileLastEdit.time) {
        url += '?'+state.MyProfileLastEdit.time.toISOString()
      }
      return url;
    },
    GET_USER_PROFILE_DEBUG: (state) => {
      return state.Debug;
    },
    GET_PROFILES: (state) => {
      return state.UserProfiles;
    },
    GET_HAS_PROFILE_LOADED: (state) => {
      return state.HasLoaded;
    },
    GET_USER_PROFILE: (state) => (userSlug) => {
      if (typeof state.UserProfiles[userSlug] != "undefined") {
        return state.UserProfiles[userSlug];
      } else {
        return state.DefaultUserProfile;
      }
    },
    GET_MY_PROFILE_CHANGES: (state) => {
      
        return state.ProfileChanges;
      
    },
    GET_IS_EDITING_MY_PROFILE: (state) => {
      
        return state.IsEditingMyProfile;
      
    },
    GET_MY_PROFILE_LAST_EDIT: (state) => {
      
        return state.MyProfileLastEdit;
      
    },
  },
  mutations: {
    ADD_DEBUG(state, payload) {
      state.Debug.push(payload);
    },
    RESET_CHANGE_PROFILE_DATA(state) {
      state.ProfileChanges = {};
    },
    CHANGE_PROFILE_DATA(state, payload) {
      Vue.set(state.ProfileChanges, payload.field, payload.newValue);
    },
    CHANGE_PROFILE_BIO(state, payload) {
      Vue.set(state.UserProfiles[payload.userSlug], 'userBio', payload.userBio);
    },
    ADD_PROFILE_DATA(state, payload) {
      state.LastSlugLoaded = payload.userSlug;
      state.HasLoaded.push(payload.userSlug);
      Vue.set(state.UserProfiles, payload.userSlug, payload);
    },
    CLEAR_PROFILE_DATA(state, payloadSlug) {
      // state.HasLoaded.push(payload.slug);
      delete state.UserProfiles[payloadSlug];
      let i;
      for (i in state.HasLoaded) {
        if (state.HasLoaded[i] == payloadSlug) {
          state.HasLoaded.splice(i);
          break;
        }
      }
    },
    SET_IS_EDITING_MY_PROFILE(state, isEditing) {      
      state.IsEditingMyProfile = isEditing;
    },
    SET_MY_PROFILE_LAST_EDIT(state, lastEdit) {      
      state.MyProfileLastEdit = lastEdit;
    },
    FORCE_PROFILE_RELOAD(state, userSlug) {
      let i;
      for (i in state.HasLoaded) {
        if (state.HasLoaded[i] == userSlug) {
          state.HasLoaded.splice(i);
          break;
        }
      }
      setTimeout(() => {
        state.HasLoaded.push(userSlug)
      }, 500);
    },
  },
  actions: {
    CHANGE_PROFILE_BIO({ commit }, payload) {
      commit('CHANGE_PROFILE_BIO', payload)
    },
    CHANGE_MY_PROFILE({ commit }, payload) {
      commit('CHANGE_PROFILE_DATA', payload)
    },
    CHANGE_IS_EDITING_MY_PROFILE({ commit }, payload) {
      commit('SET_IS_EDITING_MY_PROFILE', payload)
    },
    async FETCH_USER_PROFILE({ commit, state, rootGetters }, payload) {
      if (typeof state.UserProfiles[payload] == "undefined") {
        var url = process.env.VUE_APP_APIBASEURL + "userprofile/" + payload;

        await axios
          .get(url, rootGetters["UserStore/authHeader"])
          .then(async function (response) {
            if (typeof response.data.sk != "undefined") {
              commit("ADD_PROFILE_DATA", response.data);
              commit("ADD_DEBUG", "Successfuly fetched user profile");
            } else {
              commit("ADD_PROFILE_DATA", {});
              commit("ADD_DEBUG", "Empty user profile");
              router.push({
                name: "404 - Page not 'here'", params: {LANG: payload}
              })
            }
          })
          .catch(function (error) {
            if (error.response.status === 404){
              console.log(error.response.status)
              console.log("error.response.status")
                  router.push({
                      name: "404 - Page not 'here'", params: {LANG: payload}
                    })
          }
            commit("ADD_DEBUG", error);
          });
      }
    },
    async FETCH_USER_PROFILE_OAUTH({ commit, state, rootGetters }, payload) {
      if (typeof state.UserProfiles[payload] == "undefined") {
        var url = process.env.VUE_APP_APIBASEURL + "userprofile-o-auth/" + payload;

        await axios
          .get(url, rootGetters["UserStore/authHeader"])
          .then(async function (response) {
            if (typeof response.data.sk != "undefined") {
              commit("ADD_PROFILE_DATA", response.data);
              commit("ADD_DEBUG", "Successfuly fetched user profile");
            } else {
              commit("ADD_PROFILE_DATA", {});
              commit("ADD_DEBUG", "Empty user profile");
            }
          })
          .catch(function (error) {
            commit("ADD_DEBUG", error);
          });
      }
    },

    async UPDATE_MY_PROFILE_DATA(
      { commit, rootGetters, dispatch},
      newProfileData
    ) {
      
      var url = process.env.VUE_APP_APIBASEURL + "updatemyprofile";
     
      axios
        .put(url, newProfileData, rootGetters["UserStore/authHeader"])
        .then(function () {
          
          if (newProfileData['userBio']) {
            commit('CHANGE_PROFILE_BIO', {
              userSlug: rootGetters["UserStore/userProfile"]['slug'],
              userBio: newProfileData['userBio']
          });
          }

          commit('RESET_CHANGE_PROFILE_DATA');
          // dispatch('CHANGE_IS_EDITING_MY_PROFILE', false);
          
          // commit('FORCE_PROFILE_RELOAD', rootGetters["UserStore/userProfile"]['slug']);
    
          commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'everything' })
          dispatch('MediaStore/RESET_UPLOAD', null, {root: true})
          if(router.currentRoute.name.includes("User Profile")){
            router.push({ path: '/'+rootGetters["UserStore/userProfile"]['userlanguage']+'/user/'+rootGetters["UserStore/userProfile"]['slug'],  })
          }
          
          // var profileBackup = state.UserProfiles[rootGetters["UserStore/userProfile"]['slug']]
          // commit('CLEAR_PROFILE_DATA', rootGetters["UserStore/userProfile"]['slug']);
          // commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'everything' })
          // setTimeout(() => {
          //   commit("ADD_PROFILE_DATA", profileBackup);
          //   commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'everything' })
          // }, 500);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    UPDATE_USER_LANGUAGE({dispatch, rootState, rootGetters}, payload){
      var userslug = rootState.UserStore.userprofile.slug
      var url = process.env.VUE_APP_APIBASEURL + "preferences/updatelanguage"
      var updatedata = {
        userlanguage: payload,
        key: {
          pk: "userprofile#" + userslug,
          sk: "userprofile#" + userslug,
        }};
        axios.put(url, updatedata, rootGetters["UserStore/authHeader"]).then(res => {
          return res
        }).catch(() => {
          console.log("User language not updated")
        }).finally(() => {
            router.push({
              name: router.currentRoute.name, params: {LANG: payload}
            })
          // }
          console.log("Language updated")
          dispatch("UserStore/UPDATE_USER_LANGUAGE", payload, {root: true})
          dispatch("Languages/UPDATE_CURRENT_LOCALE", payload, {root: true});
        });}

    // async UPDATE_MY_COVER_PICTURE(
    //   { commit, rootGetters },
    //   srckey
    // ) {
    //   console.log("UPDATE_MY_COVER_PICTURE");
    //   console.log("UPDATING");
    //   var url = process.env.VUE_APP_APIBASEURL + "media/makecoverpic";
    //   var datatosend = { srckey: srckey };
    //   console.log({url, datatosend});
    //   axios
    //     .post(url, datatosend, rootGetters["UserStore/authHeader"])
    //     .then(function (response) {
    //       console.log(response);
    //       commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'coverPic' })

    //     })
    //     .catch(function (error) {
    //       console.log(error);
    //     });
    // },
    // async UPDATE_MY_PROFILE_PICTURE(
    //   {commit, rootGetters },
    //   srckey
    // ) {
    //   console.log("UPDATE_PROFILE_PICTURE");
    //   console.log("UPDATING");
    //   var url = process.env.VUE_APP_APIBASEURL + "media/makeprofilepic";
    //   var datatosend = { srckey: srckey };
    //   console.log({url, datatosend});
    //   axios
    //     .post(url, datatosend, rootGetters["UserStore/authHeader"])
    //     .then(function (response) {
    //       console.log(response);
    //       commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'profilePic' })
    //     })
    //     .catch(function (error) {
    //       console.log(error);
    //     });
    // },
    // async UPDATE_MY_BIOGRAPHY(
    //   { commit,  rootGetters },
    //   newBio
    // ) {
    //   console.log("UPDATE_MY_BIOGRAPHY");
    //   console.log("UPDATING");
    //   var url = process.env.VUE_APP_APIBASEURL + "updatemybiography";
    //   commit('CHANGE_PROFILE_BIO', {
    //     userSlug: rootGetters["UserStore/userProfile"].slug,
    //     userBio: newBio,
    //   })
    //   var pksk = `userprofile#${rootGetters["UserStore/userProfile"].slug}`; 
    //   var datatosend = { 
    //     key: {
    //       pk: pksk,
    //       sk: pksk,
    //     },
    //     userBio: newBio
    //    };
    //   console.log({url, datatosend});
    //   axios
    //     .put(url, datatosend, rootGetters["UserStore/authHeader"])
    //     .then(function (response) {
    //       console.log(response);
    //       commit('SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'userBio' })

    //     })
    //     .catch(function (error) {
    //       console.log(error);
    //     });
    // },
  },
};
