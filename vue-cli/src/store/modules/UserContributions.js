import axios from "axios";
// import axios from "axios";
import currencyData from "../../data/currencies.json";

export default {
  namespaced: true,
  state: {
    pageMode: null,
    loggedOutPayments: false,
    whaleLevel: null,
    whaleLevels: {
      silver: { name: "Silver", color: "grey lighten-3", amountInt: 1200 },
      gold: { name: "Gold" , color: "amber lighten-4", amountInt: 2400},
      diamond: { name: "Diamond" , color: "blue lighten-4", amountInt: 5000},
      immortal: { name: "Immortal" , color: "purple lighten-4", amountInt: "Contact Us"},
    },
    currency: { code: "", name: "", rate: 0, symbol: "", countries: [] },
    country: "",
    processStage: "chooseplan",
    makeAPaymentFormValues: {
      currencyCode: null,
      term: "",
      method: "",
      amount: 0,
      finalAmount: 0,
    },
    userPayments: [],
    userPaymentSchedules: [],
    paymentOptions: [
      { provider: "stripe", label: "Pay with debit/credit card" },
      { provider: "paypal", label: "Pay with Paypal" },
    ],
    paymentTerms: [
      { label: "Single", value: "once" },
      { label: "Monthly", value: "month" },
      { label: "Annual", value: "year" },
    ],
    fetchOperations: {
      payments: "idle",
      paymentschedules: "idle",
      userCountry: "idle",
    },
    anonCustomer: {
      email: null,
      fname: null,
      lname: null,
    },
  },
  getters: {
    GET_LOGGED_OUT_PAYMENTS: (state) => {
      return state.loggedOutPayments;
    },
    GET_WHALE_LEVEL: (state) => {
      return state.whaleLevel;
    },
    GET_ALL_WHALE_LEVELS: (state) => {
      return state.whaleLevels;
    },
    GET_CONTRIBUTE_PAGE_MODE: (state) => {
      return state.pageMode;
    },
    GET_IS_ANON_PAYMENT: (state, getters, rootState) => {
        return (state.loggedOutPayments == true ||
          process.env.VUE_APP_PAYMENTS_ONLY_MODE == "true") &&
          !rootState.UserStore.isLoggedIn
          ? true
          : false;
      
    },
    GET_CURRENCY: (state) => {
      return state.currency;
    },
    GET_PRICES: (state) => {
      return state.currency.amounts;
    },
    GET_USER_COUNTRY: (state) => {
      return state.country;
    },
    GET_CURRENCY_DATA: () => {
      let c;
      let list = [];
      for (c in currencyData['currencies']) {
        let currency = currencyData['currencies'][c];
        list.push({
          "label": "("+currency['symbol']+"/"+currency['code']+") "+currency['name'],
          "value": currency['code']
        })
      }
      return list;
    },
    GET_ANON_CUSTOMER: (state) => {
      return state.anonCustomer;
    },
    GET_PROCESS_STAGE: (state) => {
      return state.processStage;
    },
    GET_MAKE_A_PAYMENT_VALUES: (state) => {
      return state.makeAPaymentFormValues;
    },
    GET_USER_PAYMENTS: (state) => {
      return state.userPayments;
    },
    GET_USER_PAYMENT_SCHEDULES: (state) => {
      return state.userPaymentSchedules;
    },
    GET_PAYMENT_TERMS: (state) => {
      return state.paymentTerms;
    },
    GET_PAYMENT_OPTIONS: (state) => {
      return state.paymentOptions;
    },
    GET_CONTRIBUTION_FETCH_OPERATIONS: (state) => {
      return state.fetchOperations;
    }
  },
  mutations: {
    SET_CONTRIBUTE_MODE (state, payload) {
      state.pageMode = payload;
    },
    SET_WHALE_LEVEL (state, payload) {
      state.whaleLevel = payload;
    },
    SET_ALLOW_LOGGED_OUT_PAYMENTS (state) {
      state.loggedOutPayments = true;
    },
    
    ADD_USER_CONTRIBUTIONS(state, payload) {
      state.userContributions.push(payload);
    },

    SET_AMOUNT_TO_PAY(state, payload) {
      state.makeAPaymentFormValues.amount = payload;
    },
    SET_USER_COUNTRY(state, payload) {
      state.country = payload;
    },
    SET_CURRENCY(state, payload) {
      state.currency = payload;
      state.makeAPaymentFormValues.currencyCode = payload.code;
    },
    SET_ANON_CUSTOMER_EMAIL(state, payload) {
      state.anonCustomer.email = payload;
    },
    SET_ANON_CUSTOMER_FNAME(state, payload) {
      state.anonCustomer.fname = payload;
    },
    SET_ANON_CUSTOMER_LNAME(state, payload) {
      state.anonCustomer.lname = payload;
    },
    SET_PROCESS_STAGE(state, stringPayload) {
      state.processStage = stringPayload;
    },
    SET_FINAL_AMOUNT_TO_PAY(state, payload) {      
           
      state.makeAPaymentFormValues.finalAmount = payload;
    },
    SET_TERM_OF_PAYMENT(state, payload) {
      state.makeAPaymentFormValues.term = payload;
    },
    SET_PAYMENT_METHOD(state, payload) {
      state.makeAPaymentFormValues.method = payload;
    },
    SET_USER_PAYMENTS(state, payload) {
      state.userPayments = payload;
    },
    SET_USER_PAYMENT_SCHEDULES(state, payload) {
      state.userPaymentSchedules = payload;
    },
    SET_FETCH_OPERATIONS(state, payload) {
      state.fetchOperations[payload["item"]] = payload["value"];
    },
  },
  actions: {
    ALLOW_LOGGED_OUT_PAYMENTS({ commit }) {
      commit('SET_ALLOW_LOGGED_OUT_PAYMENTS');
    },
    CHANGE_CURRENCY({ commit, dispatch}, payload) {
      let i;
      for (i in currencyData.currencies) {
        if (currencyData.currencies[i].code == payload) {
          commit("SET_CURRENCY", currencyData.currencies[i]);
          dispatch('CHOOSE_DEFAULT_AMOUNT')
        }
      }
    },
    CHANGE_PROCESS_STAGE({ commit }, stringPayload) {
      commit("SET_PROCESS_STAGE", stringPayload);
    },
    CHANGE_WHALE_LEVEL({ commit, state }, newLevel) {
      commit("SET_WHALE_LEVEL", newLevel);
      if (state.whaleLevels[newLevel]) {
        commit("SET_TERM_OF_PAYMENT", "year");
        commit("SET_AMOUNT_TO_PAY", "Other");
        commit("SET_FINAL_AMOUNT_TO_PAY", `${ state.whaleLevels[state.whaleLevel].amountInt }`);
      }
    },
    UPDATE_AMOUNT_TO_PAY({ commit }, payload) {
      commit("SET_AMOUNT_TO_PAY", payload);

      if (payload != "Other") {
        commit("SET_FINAL_AMOUNT_TO_PAY", payload);
      }
    },
    UPDATE_ANON_CUSTOMER_EMAIL({ commit }, payload) {
      commit("SET_ANON_CUSTOMER_EMAIL", payload);
    },
    UPDATE_ANON_CUSTOMER_FNAME({ commit }, payload) {
      commit("SET_ANON_CUSTOMER_FNAME", payload);
    },
    UPDATE_ANON_CUSTOMER_LNAME({ commit }, payload) {
      commit("SET_ANON_CUSTOMER_LNAME", payload);
    },
    UPDATE_FINAL_AMOUNT_TO_PAY({ commit }, payload) {
      if (payload > 0) {
        commit("SET_FINAL_AMOUNT_TO_PAY", payload);
      }
    },
    UPDATE_TERM_OF_PAYMENT({ commit, dispatch }, payload) {
      commit("SET_TERM_OF_PAYMENT", payload);
      dispatch("CHOOSE_DEFAULT_AMOUNT")

    },
    CHOOSE_DEFAULT_AMOUNT({ state, dispatch }) {
      console.log('CHOOSE_DEFAULT_AMOUNT')
      let amount = 0;
      console.log(state.currency.amounts);
      if ( state.makeAPaymentFormValues.term == 'month') {
        amount = state.currency.amounts['monthly'][1]
      } else if ( state.makeAPaymentFormValues.term == 'year') {
        amount = state.currency.amounts['annual'][1]
      
      } else {
        amount = state.currency.amounts['oneoff'][1]
        
      }
      dispatch('UPDATE_AMOUNT_TO_PAY', amount)
    },
    UPDATE_PAYMENT_METHOD({ commit }, payload) {
      commit("SET_PAYMENT_METHOD", payload);
    },
    async FETCH_USER_PAYMENTS({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/mine";
      commit("SET_FETCH_OPERATIONS", {
        item: "payments",
        value: "fetching",
      });

      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_USER_PAYMENTS", response.data);
          commit("SET_FETCH_OPERATIONS", {
            item: "payments",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_FETCH_OPERATIONS", {
            item: "payments",
            value: "error",
          });
          console.log(error);
        });
    },
    async FETCH_USER_COUNTRY_AND_MAKE_CURRENCY({ commit, state, dispatch }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "getuserscountry";
      commit("SET_FETCH_OPERATIONS", {
        item: "userCountry",
        value: "fetching",
      });

      axios
        .get(url)
        .then(function (response) {
          commit("SET_USER_COUNTRY", response.data.country);
          commit("SET_FETCH_OPERATIONS", {
            item: "userCountry",
            value: "success",
          });

          let c;
          let found = false;
          for (c in currencyData['currencies']) {
            let currency = currencyData['currencies'][c];
            let o;
            for (o in currency['countries']) {
              if (currency['countries'][o]['alpha-2'] == state.country) {
                found = true;
                commit("SET_CURRENCY", currency);
                dispatch("CHOOSE_DEFAULT_AMOUNT")

                break;
              }
            }
            if (found) {
              break;
            }
          }



        })
        .catch(function (error) {
          commit("SET_FETCH_OPERATIONS", {
            item: "userCountry",
            value: "error",
          });
          console.log(error);
        });
    },
    async FETCH_USER_PAYMENT_SCHEDULES({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "paymentschedules/mine";
      commit("SET_FETCH_OPERATIONS", {
        item: "paymentschedules",
        value: "fetching",
      });

      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_USER_PAYMENT_SCHEDULES", response.data);
          commit("SET_FETCH_OPERATIONS", {
            item: "paymentschedules",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_FETCH_OPERATIONS", {
            item: "paymentschedules",
            value: "error",
          });
          console.log(error);
        });
    },
    CREATE_NEW_PAYMENT({ rootGetters }, payload) {
      var url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/add";

      axios
        .post(url, payload, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async CREATE_NEW_SCHEDULE({ rootGetters }, payload) {
    

      var url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/schedules/add";

      axios
        .post(url, payload, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async CANCEL_SCHEDULE({ rootGetters }, payload) {
  
      var url =
        process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/schedules/cancel/" + payload;

      axios
        .post(url, payload, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    DETECT_CONTRIBUTE_MODE({ commit }, routeName) {
    
      if (routeName == "Standard Contribution") {
        commit('SET_CONTRIBUTE_MODE', 'standard');
      } else if (routeName == "Whales Contribution") {
        commit('SET_CONTRIBUTE_MODE', 'whales');
      } else if (routeName == "Social Contribution") {
        commit('SET_CONTRIBUTE_MODE', 'socialmedia');
      }
    }
  },
  
};
