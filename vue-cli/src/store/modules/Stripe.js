import axios from "axios";
// import { loadStripe } from "@stripe/stripe-js";

export default {
  namespaced: true,
  state: {
    error: false,
    paymentResult: {
      paymentIntent: {
        amount: 0,
        id: null,
      },
    },
    errorMessage: [],
    stripeCustomer: {},
    stripePriceData: {},
    stripeInvoices: {},
    stripePaymentIntents: {},
    stripeNewSubscription: {},
    stripeSubscriptions: {},
    fetchOperations: {
      customer: "waiting",
      intent: "waiting",
      invoices: "waiting",
      price: "waiting",
      paymentIntents: "waiting",
      subscriptionCreate: "waiting",
      subscriptions: "waiting",
    },
  },
  getters: {
    GET_PAYMENTERROR: (state) => {
      return state.error;
    },
    GET_PAYMENTRESULT: (state) => {
      return state.paymentResult;
    },
    GET_PAYMENTRESULT_ID: (state) => {
      return state.paymentResult.paymentIntent.id;
    },
    GET_STRIPE_CUSTOMER: (state) => {
      return state.stripeCustomer;
    },
    GET_STRIPE_PRICE_DATA: (state) => {
      return state.stripePriceData;
    },
    GET_STRIPE_INVOICES: (state) => {
      return state.stripeInvoices;
    },
    GET_STRIPE_PAYMENT_INTENTS: (state) => {
      return state.stripePaymentIntents;
    },
    GET_STRIPE_NEW_SUBSCRIPTION: (state) => {
      return state.stripeNewSubscription;
    },
    GET_STRIPE_SUBSCRIPTIONS: (state) => {
      return state.stripeSubscriptions;
    },
    GET_STRIPE_FETCH_OPERATIONS: (state) => {
      return state.fetchOperations;
    },
    GET_STRIPE_ERROR_MESSAGE: (state) => {
      return state.errorMessage;
    },
  },
  mutations: {
    SET_PAYMENTERROR(state, payload) {
      state.error = payload;
    },
    SET_PAYMENTRESULT(state, payload) {
      state.paymentResult = payload;
    },
    SET_STRIPE_CUSTOMER(state, payload) {
      state.stripeCustomer = payload;
    },
    SET_STRIPE_PRICE_DATA(state, payload) {
      state.stripePriceData = payload;
    },
    SET_STRIPE_INVOICES(state, payload) {
      state.stripeInvoices = payload;
    },
    SET_STRIPE_PAYMENT_INTENTS(state, payload) {
      state.stripePaymentIntents = payload;
    },
    SET_STRIPE_NEW_SUBSCRIPTION(state, payload) {
      state.stripeNewSubscription = payload;
    },
    SET_STRIPE_SUBSCRIPTIONS(state, payload) {
      state.stripeSubscriptions = payload;
    },
    SET_STRIPE_FETCH_OPERATIONS(state, payload) {
      if (payload["message"]) {
        state.errorMessage = payload["message"];
      }
      state.fetchOperations[payload["item"]] = payload["value"];
    },
  },
  actions: {
    PUSH_PAYMENTERROR({ commit }, payload) {
      commit("SET_PAYMENTERROR", payload);
    },
    PUSH_PAYMENTRESULT({ commit }, payload) {
      commit("SET_PAYMENTRESULT", payload);
    },
    // async INIT_PAYMENT_INTENT({commit}) {
    //   const stripe = await loadStripe(process.env.VUE_APP_STRIPE_KEY);
    //   console.log(this.userProfile);
    //   var purchase = {
    //     slug: this.userProfile.slug,
    //     amount: this.stripeAmount,
    //     currency: this.GET_CURRENCY.code,
    //   };
    //   const self = this;
    //   this.isLoading = true;
    //   fetch(process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/intent", {
    //     method: "POST",
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //     body: JSON.stringify(purchase),
    //   })
    //     .then(function (result) {
    //       return result.json();
    //     })
    //     .then(function (data) {
    //       self.isLoading = false;
    //       const elements = stripe.elements();
    //       const style = {
    //         base: {
    //           color: "#fff",
    //           fontFamily: "Poppins, sans-serif",
    //           fontSmoothing: "antialiased",
    //           fontSize: "16px",
    //           ":-webkit-autofill": {
    //             color: "#fff",
    //           },
    //           "::placeholder": {
    //             color: "#fff",
    //           },
    //         },
    //         invalid: {
    //           fontFamily: "Poppins, sans-serif",
    //           color: "#AB3F27",
    //           iconColor: "#AB3F27",
    //         },
    //       };
    //       const card = elements.create("card", { style: style });
    //       // Stripe injects an iframe into the DOM
    //       card.mount("#stripe-card-element");
    //       card.on("change", function (event) {
    //         self.formValid = !event.empty && event.complete;
    //         //self.formValid = event.error;
    //         self.errorMessage = event.error ? event.error.message : "";
    //       });
    //       var form = document.getElementById("payment-form");
    //       form.addEventListener("submit", function (event) {
    //         event.preventDefault();
    //         // Complete payment when the submit button is clicked
    //         self.payWithCard(stripe, card, data.client_secret);
    //       });
    //     });
    // },
    async CREATE_SUBSCRIPTION_DATA({ commit, state, rootGetters }) {

      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "subscriptionCreate",
        value: "fetching",
      });
   

      let url, headers, data;
      if (rootGetters['UserContributionsStore/GET_IS_ANON_PAYMENT']) {
        url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/subscriptionanon";
        headers = {
          "content-type": "application/json",
        };
        data = {
          stripePriceId: state.stripePriceData.priceId,
          currency: state.stripePriceData.currency,
          ...rootGetters['UserContributionsStore/GET_ANON_CUSTOMER']
        };

      } else {
        url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/subscription";
        headers = rootGetters["UserStore/authHeader"];
        data = {
          stripePriceId: state.stripePriceData.priceId,
          currency: state.stripePriceData.currency,
        };
      }


      await axios
        .post(
          url,
          data,
          headers
        )
        .then(function (response) {

          commit("SET_STRIPE_NEW_SUBSCRIPTION", response.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptionCreate",
            value: "success",
          });
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response.data); // => the response payload
          }
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptionCreate",
            value: "error",
            message: error.response ? error.response.data.message : null,
          });
          // console.log(error);
        });
    },
    async CANCEL_SUBSCRIPTION_DATA(
      { commit, rootGetters },
      subscriptionIdPayload
    ) {
      var url;
      url =
        process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/cancelsubscription";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "subscriptionCancel",
        value: "fetching",
      });
      
      await axios
        .post(
          url,
          {
            subscriptionId: subscriptionIdPayload,
          },
          rootGetters["UserStore/authHeader"]
        )
        .then(function () {
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptionCancel",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptionCancel",
            value: "error",
          });
          console.log(error);
        });
    },
    async FETCH_STRIPE_PRICE_DATA({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/pricedata";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "price",
        value: "fetching",
      });
      if (
        rootGetters["UserContributionsStore/GET_MAKE_A_PAYMENT_VALUES"]
          .finalAmount <= 0.3
      ) {
        return false;
      }
      let interval =
        rootGetters["UserContributionsStore/GET_MAKE_A_PAYMENT_VALUES"].term;
      await axios
        .post(
          url,
          {
            currency: rootGetters["UserContributionsStore/GET_CURRENCY"].code,
            amount:
              rootGetters["UserContributionsStore/GET_MAKE_A_PAYMENT_VALUES"]
                .finalAmount * 100,
            type: "recurring",
            interval: interval,
          }
        )
        .then(function (response) {
          commit("SET_STRIPE_PRICE_DATA", response.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "price",
            value: "success",
          });
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "price",
            value: "error",
          });
        });
    },
    async FETCH_STRIPE_CUSTOMER_DATA({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/customer";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "customer",
        value: "fetching",
      });

      axios
        .post(url, null, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_STRIPE_CUSTOMER", response.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "customer",
            value: "success",
          });
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "customer",
            value: "error",
          });
        });
    },
    async FETCH_STRIPE_INVOICE_DATA({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/invoices";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "invoices",
        value: "fetching",
      });

      axios
        .post(url, null, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_STRIPE_INVOICES", response.data.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "invoices",
            value: "success",
          });
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "invoices",
            value: "error",
          });
        });
    },
    async FETCH_STRIPE_PAYMENT_INTENT_DATA({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/paymentintents";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "paymentIntents",
        value: "fetching",
      });

      axios
        .post(url, null, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_STRIPE_PAYMENT_INTENTS", response.data.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "paymentIntents",
            value: "success",
          });
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "paymentIntents",
            value: "error",
          });
        });
    },
    async FETCH_STRIPE_SUBSCRIPTION_DATA({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/stripe/subscriptions";
      commit("SET_STRIPE_FETCH_OPERATIONS", {
        item: "subscriptions",
        value: "fetching",
      });

      axios
        .post(url, null, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_STRIPE_SUBSCRIPTIONS", response.data.data);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptions",
            value: "success",
          });
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_STRIPE_FETCH_OPERATIONS", {
            item: "subscriptions",
            value: "error",
          });
        });
    },
  },
};
