import i18n from "@/plugins/i18n";
// import axios from "axios";

export default {
    namespaced: true,
    state: {
        languages: [
            { flag: "us", language: "en", title: "English" },
            { flag: "es", language: "es", title: "Español" }, 
            { flag: 'de', language: 'de', title: 'Deutsch' },
            { flag: 'br', language: 'pt', title: 'Português' },
            { flag: 'ru', language: 'ru', title: 'Русский' }
        ],
        languagetitles: ['English', 'Español', 'Deutsch', 'Português', 'Русский'],
        languagecode: "en",
        languagename: "English"
    },
    actions: {
        UPDATE_SESSION_LANG({ commit }, payload) {
            commit("SET_NEW_SESSION_LANG", payload);
        },
        SET_USER_LANG_CODE({ commit }, payload) {
            commit("SET_USER_LANG_CODE", payload)
        },
        UPDATE_CURRENT_LOCALE({commit}, payload){
            if(payload === ""){
                payload = localStorage.locale
            }
            commit("UPDATE_CURRENT_LOCALE", payload)
        },
        UPDATE_CURRENT_LOCALE_COPY({commit}, payload){
            commit("UPDATE_CURRENT_LOCALE_COPY", payload)
        },
        // GET_LOCALE_COPY({state}, payload){
        //     let i18nMessagesKeyArray = Object.keys(i18n.messages)
        //     i18n.locale = payload
        //     if(payload === localStorage.locale && Object.keys(i18n.messages).length != 0 && i18nMessagesKeyArray[0] === payload){
        //         console.log(i18n.messages)    
        //         console.log("GET_LOCALE_COPY - We have the right messages")
        //             return i18n.messages
        //         }
        //     else {
        //         var getlanguagename = state.languages.filter(obj => {
        //             return obj.language === payload
        //             })
        //         state.languagename = getlanguagename[0]?.title
        //         const locale = state.languagename
        //         var url = process.env.VUE_APP_APIBASEURL + "getlocalecopy"
        //         var retrievaldata = {
        //               key: locale,
        //         };
        //         axios.post(url, retrievaldata).then(res => {
        //           i18n.setLocaleMessage(i18n.locale, res.data[0] || {});
        //         }).catch(() => {
        //           console.log("Messages were not retrieved")
        //         }).finally(() => {
        //           console.log("We have the messages")
        //           console.log(i18n.messages)
        //           localStorage.setItem("locale", payload)
        //         });}
        //         }
    },
    mutations: {
        SET_USER_LANG_CODE(state, payload){
            var getlanguagenameCode = state.languages.filter(obj => {
                return obj.title === payload
              })
            state.languagecode = getlanguagenameCode[0].language
        },
        SET_NEW_SESSION_LANG(state, payload) {
            i18n.locale = payload.language
            localStorage.setItem("locale", i18n.locale.toString());
            state.languagename = payload.title
            state.languagecode = payload.language
            localStorage.setItem("languagename", payload.title.toString())
        },
        UPDATE_CURRENT_LOCALE(state, payload){
            i18n.locale = payload
            state.languagecode = payload
            localStorage.setItem("locale", i18n.locale)
            var getlanguagename = state.languages.filter(obj => {
                return obj.language === payload
              })
            state.languagename = getlanguagename[0]?.title
        },},
    getters: {
        GET_LOCALE() {
            return i18n.locale;
        },
        GET_LANG_LIST(state) {
            return state.languages;
        },
        GET_CURRENT_LANGUAGE(state) {
            return state.languagename
        },
        GET_USER_LANG_CODE(state) {
            return state.languagecode
        },
        GET_LANG_TITLES(state){
            return state.languagetitles
        }
    }
};