import Vue from "vue";
import axios from "axios";

import createPersistedState from "vuex-persistedstate";
import Vuetify from "@/plugins/vuetify";

export default {
  namespaced: true,
  plugins: [createPersistedState()],
  state: {
    defaultHomepageTab: "everything",
    isDarkMode: ""
  },
  getters: {
    getPrefDefaultHomepageTab: (state) => {
      return state.defaultHomepageTab;
    },
    IS_DARK_MODE() {
      return Vuetify.framework.theme.dark;
    },
    IS_DARK_MODE_STORE(state){
      return state.isDarkMode
    }
  },
  mutations: {
    cacheStore(state) {
      localStorage.setItem("preferences", JSON.stringify(state));
    },
    setOnePref(state, payload) {
      Vue.set(state, payload["key"], payload["value"]);
    },
    setAllPrefs(state, payload) {
      let i;
      let keys = Object.keys(payload);
      let darkModeChanged = false;
      let darkMode = false;
      for (i in keys) {
        let k = keys[i];
        state[k] = payload[k];
        if (k == 'isDarkMode') {
          darkModeChanged = true;
          darkMode = payload[k]
        }
      }
      if (darkModeChanged) {
        setTimeout(() => {
          Vuetify.framework.theme.dark = darkMode;
        }, 10);
      }
    },
    clearAllPrefs(state) {
      state.defaultHomepageTab = "everything";
    },
  },
  actions: {
    LOAD_PREFERENCES_CACHE({ commit }) {
      if (localStorage.getItem("preferences")) {
        let preferences = JSON.parse(localStorage.getItem("preferences"));
        commit("setAllPrefs", preferences);
      }
    },
    CLEAR_PREFERENCES({ commit }) {
      commit("clearAllPrefs");
    },
    FETCH_PREFERENCES({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_AUTHAPIBASEURL + "preferences/getmypreferences";
      axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("setAllPrefs", response.data.generalPreferences);
          commit("cacheStore");
        })
        .catch(function (error) {
          console.log("Preferences fetch response:");
          console.log(error);
        });
    },
    SAVE_PREFERENCE({ rootGetters, commit }, payload) {
      var putdata = { [payload["key"]]: payload["value"] };
      commit("setOnePref", { key: payload["key"], value: payload["value"] });
      commit("cacheStore");
      var url =
        process.env.VUE_APP_AUTHAPIBASEURL + "preferences/" + payload["endpoint"];
      axios
        .put(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function () {})
        .catch(function (error) {
          console.log(error);
        });
    },
  },
};
