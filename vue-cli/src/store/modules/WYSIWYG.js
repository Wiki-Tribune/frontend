// TODO: Put the mention plugin back once adapted
export default {
  namespaced: true,
  state: {
    mceKey: "77q4gfh46e7hmkvwagx60kkaouduyviq06ra0b5te95vridg",
    editorSettings: {
      admin: {
        placeholder: "Enter the content to appear",
        plugins: [
          "image emoticons anchor paste advlist autolink lists link charmap print preview wordcount",
          "searchreplace visualblocks code fullscreen",
          "paste code help wordcount image hr",
        ],
        toolbars:
          "anchor pastetext | undo redo | formatselect | bold italic emoticons | \
           removeformat | help",
        menubar: "insert",
        height: 500,
      },
      post: {
        placeholder: "Ey up doc? Tell us what's going on...",
        plugins: [
          "image emoticons anchor paste advlist autolink lists link charmap print preview wordcount",          
          "paste code help hr",
        ],
        toolbars:
          "image link emoticons | bold italic \
          pastetext removeformat | help | undo redo",
        menubar: "",
        height: 250,
      },
      edit: {
        placeholder: "Enter the new text",
        plugins: [
          "image emoticons anchor paste advlist autolink lists link charmap print preview wordcount",          
          "paste code help hr",
        ],
        toolbars:
          "image link emoticons | bold italic \
          pastetext removeformat | help | undo redo",
        menubar: "",
        height: 250,
      },
      comment: {
        placeholder: "Your comment",
        plugins: [
          "image emoticons anchor paste advlist autolink lists link charmap print preview wordcount",          
          "paste code help hr",
        ],
        toolbars:
          "image link emoticons | bold italic \
          pastetext removeformat | help | undo redo",
        menubar: "",
        height: 150,
      },
    },
    contentSources: {
      post: "PostStore/GET_NEW_POST_TEXT",
      comment: "CommentStore/GET_NEW_COMMENT_TEXT",
      edit: "EditStore/GET_NEW_TEXT",
    },
  },
  getters: {
    GET_CONTENT_SOURCES: (state) => {
      return state.contentSources;
    },
    GET_CONTENT:
      (state, getters, rootState, rootGetters) => (modeOfUse, identifier) => {
        if (modeOfUse == "comment") {
          return rootGetters[state.contentSources[modeOfUse]](identifier);
        } else {
          return rootGetters[state.contentSources[modeOfUse]];
        }
      },
    GET_MCEKEY: (state) => {
      return state.mceKey;
    },
    GET_MCE_SETTING: (state) => (useCase, setting) => {
      if (
        typeof state.editorSettings[useCase] != "undefined" &&
        typeof state.editorSettings[useCase][setting] != "undefined"
      ) {
        return state.editorSettings[useCase][setting];
      } else {
        return false;
      }
    },
  },
  mutations: {},
  actions: {
    UPDATE_CONTENT({ dispatch }, payload) {

      if (payload.identifier == "PostCreate") {
        dispatch("PostStore/UPDATE_NEW_POST_TEXT", payload.editorContent, {
          root: true,
        });
      } else if (payload.modeOfUse == "comment") {
        dispatch(
          "CommentStore/UPDATE_NEW_COMMENT_TEXT",
          {
            identifier: payload.identifier,
            commentText: payload.editorContent,
          },
          {
            root: true,
          }
        );
      } else if (payload.identifier == "Edit") {
        dispatch("EditStore/UPDATE_NEW_TEXT", payload.editorContent, {
          root: true,
        });
      }
    },
  },
};
