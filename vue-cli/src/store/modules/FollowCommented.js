// This is also not referenced anywhere and just for Fiona to comment on 

// Importing axios - https://axios-http.com/ - Promise based HTTP client for the browser and node.js
import axios from "axios";

export default {
    // namespace allows you to use the same method names for different modules
    // because otherwise that's just eurgh
    namespaced: true,
    state: {
        // Debug is an empty array you can add to
        Debug: [],
        // This is an array of all subwikis and user profiles a user has followed
        // throughout their time on the website because Dynamo
        followMemory: {
            subwiki: [],
            userprofile: [],
        },
    },
    // Functions that we would like to call like a property (getters)
    getters: {
        GET_DEBUG: (state) => {
            return state.Debug;
        },
        // 
        GET_FOLLOW_STATUS: (state) => (payload) => {
            console.log(`GET_FOLLOW_STATUS: "${payload.type}/${payload.slug}"`);
            console.log("state.followMemory:");
            console.log(state.followMemory);
            if (payload.type && payload.slug) {
                let found = { following: false };
                let i;
                if (state.followMemory[payload.type]) {
                    console.log(state.followMemory[payload.type]);

                    for (i in state.followMemory[payload.type]) {
                        console.log(i);
                        console.log(state.followMemory[payload.type][i]);
                        if (
                            state.followMemory[payload.type][i].followSlug == payload.slug
                        ) {
                            console.log("Found!");
                            found = state.followMemory[payload.type][i];
                            break;
                        } else {
                            console.log(
                                `${state.followMemory[payload.type][i].followSlug} != ${
                  payload.slug
                }`
                            );
                        }
                    }
                } else {
                    console.log("Unregonised type: " + payload.type);
                }
                console.log("found:");
                console.log(found);
                return found;
            }
        },
    },
    mutations: {
        ADD_DEBUG(state, payload) {
            state.Debug.push(payload);
        },

        CHANGE_FOLLOW_MEMORY(state, payload) {
            console.log("CHANGE_FOLLOW_MEMORY");
            console.log(payload);
            if (state.followMemory[payload.type]) {
                if (payload.type && payload.slug) {
                    let i;
                    if (state.followMemory[payload.type]) {
                        console.log(state.followMemory[payload.type]);

                        for (i in state.followMemory[payload.type]) {
                            console.log(i);
                            console.log(state.followMemory[payload.type][i]);
                            if (state.followMemory[payload.type][i].slug == payload.slug) {
                                delete state.followMemory[payload.type][i];
                            }
                        }
                    }
                }

                state.followMemory[payload.type].push({
                    following: payload.following,
                    slug: payload.slug,
                    followSlug: payload.followSlug,
                });
            } else {
                console.log("Unknown follow type: " + payload.type);
            }
            console.log(state.followMemory);
        },
    },
    actions: {
        UPDATE_EDIT_COMMENTSFOR({ commit }, payload) {
            commit("SET_EDIT_COMMENTSFOR", payload);
        },
        FETCH_FOLLOW_STATUS({ commit, rootState }, payload) {
            console.log("FETCH_FOLLOW_STATUS");
            console.log(payload);
            console.log("creator: " + rootState.UserStore.userprofile.slug);
            if (payload) {
                var url;
                url =
                    process.env.VUE_APP_APIBASEURL +
                    "relfollow/" +
                    payload.type +
                    "_" +
                    payload.slug.trim() +
                    "_" +
                    rootState.UserStore.userprofile.slug;

                console.log("url: " + url);
                axios
                    .get(encodeURI(url), {
                        headers: { "content-type": "application/json" },
                    })
                    .then(function(response) {
                        console.log("Unfollow response:");
                        console.log(response);
                        commit("CHANGE_FOLLOW_MEMORY", {
                            slug: url,
                            type: response.data.followType,
                            followSlug: response.data.parentSlug,
                            following: true,
                        });
                    })
                    .catch(function(error) {
                        console.log("Follow response:");
                        console.log(error);

                        commit("ADD_DEBUG", error);
                    });
            }
        },
        CREATE_FOLLOW_RELATIONSHIP({ commit, rootState }, payload) {
            console.log("CREATE_FOLLOW_RELATIONSHIP");
            console.log(payload);

            var url = process.env.VUE_APP_APIBASEURL + "relfollow";
            var putdata = {
                followSlug: payload.slug,
                followType: payload.type,
            };
            console.log(putdata);
            commit("CHANGE_FOLLOW_MEMORY", {
                slug: payload.type +
                    "_" +
                    payload.slug.trim() +
                    "_" +
                    rootState.UserStore.userprofile.slug,
                type: payload.type,
                followSlug: payload.slug,
                following: true,
            });
            axios
                .post(url, putdata, {
                    headers: { "content-type": "application/json" },
                })
                .then(function(response) {
                    console.log(response);
                    if (typeof response.data.slug != "undefined") {
                        commit("CHANGE_FOLLOW_MEMORY", {
                            slug: response.data.slug,
                            type: response.data.followType,
                            followSlug: response.data.parentSlug,
                            following: true,
                        });
                    } else {
                        console.log("No id");
                    }
                    commit("ADD_DEBUG", response);
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                });
        },
        DELETE_FOLLOW_RELATIONSHIP({ commit, rootState }, payload) {
            console.log("DELETE_FOLLOW_RELATIONSHIP");

            let slug =
                payload.type +
                "_" +
                payload.slug.trim() +
                "_" +
                rootState.UserStore.userprofile.slug;
            var url = process.env.VUE_APP_APIBASEURL + "relfollow/" + slug;
            commit("CHANGE_FOLLOW_MEMORY", {
                slug: slug,
                type: payload.type,
                followSlug: payload.slug,
                following: false,
            });
            console.log(url);
            axios
                .delete(url, {
                    headers: { "content-type": "application/json" },
                })
                .then(function(response) {
                    console.log(response);
                    commit("ADD_DEBUG", response);
                })
                .catch(function(error) {
                    commit("ADD_DEBUG", error);
                });
        },
    },
};