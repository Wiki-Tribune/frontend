import router from "../../router/index";

export default {
  namespaced: true,
  state: {
    lastConnect: null,
    closing: false,
    WSconnection: null,
    isConnected: false,
    isReconnected: false,
    error: null,
    reconnectionAttemps: 0,
  },
  getters: {
    isSocketConnected: (state) => state.isConnected,
    isSocketReconnected: (state) => state.isReconnected,
  },
  mutations: {

    SOCKET_ON_CONNECTED: (state, ws) => {
      console.log("Websocket Connected");
      state.closing = false;
      state.WSconnection = ws;
      state.error = null;
      state.isConnected = true;
    },
    SOCKET_ON_DISCONNECT(state, errorReason) {
      console.log("Websocket Disconnected");
      state.isConnected = false;
      state.isReconnected = false;
      if (errorReason) {
        state.error = errorReason;
      }
    },
    SOCKET_ON_ERROR(state, errorMessage) {
      console.log("Websocket Error");
      state.error = errorMessage;
    },
    SOCKET_ON_MESSAGE(state, message) {
      state.socket.message = message;
    },
    SOCKET_ON_RECONNECTED(state, ws) {
      console.log("Websocket ReConnected");
      state.WSconnection = ws;
      state.error = null;
      state.isReconnected = true;
      state.isConnected = true;
      state.lastConnect = new Date();
    },
    SOCKET_ON_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
    SOCKET_SET_CONNECTION(state, newConnection) {
      state.WSconnection = newConnection;
    },
    SOCKET_SET_DESTROYED(state) {
      state.closing = true;
      if (state.WSconnection) {
        state.WSconnection.close();
        state.WSconnection = null;
        state.isConnected = false;
        state.error = null;
      }
    },
    SOCKET_SET_CLOSING(state, closingState) {
      state.closing = closingState;
    },
    ADD_RECONNECT_ATTEMPT(state) {
      state.reconnectionAttemps++;
    },

  },
  actions: {
    SOCKET_DESTROY({ commit }) {
      commit("SOCKET_SET_DESTROYED");
    },
    SOCKET_SEND_MESSAGE({ state }, payload) {
      if (state.WSconnection) {
        state.WSconnection.send(JSON.stringify(payload));
      }
    },
    SOCKET_CONNECT: (
      { commit, dispatch, state, rootGetters },
      isReconnecting
    ) => {
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      console.log("Attempting to connect to websocket")
      commit('ADD_RECONNECT_ATTEMPT');
      if (state.lastConnect) {
        var now = new Date();
        var dif = now.getTime() - state.lastConnect.getTime();
        if (dif < 1 && state.reconnectionAttemps > 5) {
          console.log('Socket connection attempts too frequent - delaying for 60 seconds');
          setTimeout(() => {
            dispatch('SOCKET_CONNECT');
          }, 60000);
        }

      }


      const host = new URL(process.env.VUE_APP_WEBSOCKETURL)
      host.searchParams.set('access_token', rootGetters["UserStore/userToken"])
      if (!rootGetters["UserStore/userToken"]) {
        console.log("No token")
        return false;
      }
      
      var ws = new WebSocket(host);
      ws.onopen = function () {
        if (isReconnecting) {
          commit("SOCKET_ON_RECONNECTED", ws);
        } else {
          commit("SOCKET_ON_CONNECTED", ws);
        }
        dispatch("SocketStore/SOCKET_SEND_MESSAGE", {
          action: 'CLIENT_CHANGE_PAGE',
          payload: {
            path: router.currentRoute.fullPath
          }
        }, { root: true });
        ws.onmessage = function (e) {
          const parsedData = JSON.parse(e.data);
          if (parsedData.action && parsedData.namespace) {
            dispatch(
              `${parsedData.namespace}/${parsedData.action}`,
              parsedData.payload,
              { root: true }
            );
            
          }
        };
      };

      ws.onclose = function (e) {
        commit("SOCKET_ON_DISCONNECT", e.reason);
        console.log("SOCKET_ON_DISCONNECT");
        if (!state.closing) {
          ws = null;
          setTimeout(() => {
            console.log("SOCKET_CONNECT");
            return dispatch("SOCKET_CONNECT", true);
          }, 60000);
        } else {
          commit("SOCKET_SET_CLOSING", false);
        }
      };
    },
  },
};
