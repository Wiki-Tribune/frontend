import axios from "axios";
import router from "../../router/index";
import Vue from "vue";
const channel = new BroadcastChannel("oauth");

const cryptGen = require("crypto");
export default {
  namespaced: true,
  state: {
    authPageTab: 0,
    emailToConfirm: null,
    registered: false,
    status: "",
    accessTimeOut: "",
    accessToken: "",
    refreshToken: "",
    // token: "",
    generatedSlug: "",
    newSlugIsUnique: false,
    userprofile: {
      userID: "",
      lname: "",
      fname: "",
      slug: "",
      userlanguage: "",
      trustLevelInfo: {
        trustLevelInt: 0,
        trustLevelName: "L0",
        aka: "Newbie",
        pointsCanGive: 0,
        pointsRequired: 0,
      },
      votevalue: 0
    },
    errors: {
      emailconfirmation: null,
      passwordreset: {},
      changepassword: {},
      login: {},
      register: {},
      refresh: {},
    },
    resendConfirmationRequestedAt: null,
    resetTokenRequestedAt: null,
    resetTokenValidation: "waiting",
    resetPasswordState: "waiting",
    PKCECodeChallenge: null,
    userIsOnline: navigator.onLine,
    tokenRefreshState: "idle",
    initalTokenValidation: false,
    lastTokenRefreshAt: null,
    redirectAfterAuth: null,
    isLoggingOut: false,
  },
  getters: {
    authPageTab: (state) => {
      return state.authPageTab;
    },

    lastTokenRefreshAt: (state) => {
      return state.lastTokenRefreshAt;
    },
    generatedSlug: (state) => {
      return state.generatedSlug;
    },
    newSlugIsUnique: (state) => {
      return state.newSlugIsUnique;
    },
    resetTokenValidation: (state) => {
      return state.resetTokenValidation;
    },
    resetPasswordState: (state) => {
      return state.resetPasswordState;
    },
    userProfile: (state) => {
      return state.userprofile;
    },
    userSlug: (state) => {
      return state.userprofile["slug"];
    },
    userTrustLevelInfo: (state) => {
      return state.userprofile["trustLevelInfo"];
    },
    userVoteValue: (state) => {
      return state.userprofile["votevalue"];
    },
    userLanguage: (state) => {
      if (state.userprofile?.["userlanguage"]) {
        return state.userprofile["userlanguage"];
      } else if (router.currentRoute.params.LANG) {
        return router.currentRoute.params.LANG;
      } else {
        return "en";
      }
    },
    userToken: (state) => {
      return state.accessToken;
    },
    refreshToken: (state) => {
      return state.refreshToken;
    },
    tokenRefreshState: (state) => {
      return state.tokenRefreshState;
    },
    accessTimeOut: (state) => {
      return state.accessTimeOut;
    },
    dateWhenTimeout: (state) => {
      return new Date(parseInt(state.accessTimeOut));
    },
    secondsUntilTokenTimeout: (state) => {
      return (
        (new Date(parseInt(state.accessTimeOut)).getTime() -
          new Date().getTime()) /
        1000
      );
    },
    isTokenWithinValidRange: (state) => {
      return new Date().getTime() < state.accessTimeOut;
    },
    initalTokenValidation: (state) => {
      return state.initalTokenValidation;
    },
    isLoggedIn: (state) => {
      if (
        state.accessToken &&
        state.accessToken.length > 0 &&
        state.userprofile.userID != "guest"
      ) {
        return true;
      } else {
        return false;
      }
    },
    hasValidGuestToken: (state) => {
      if (
        state.accessToken &&
        state.accessToken.length > 0 &&
        state.userprofile.userID == "guest"
      ) {
        return true;
      } else {
        return false;
      }
    },
    isEmailVerified: (state) => {
      if (!state.userprofile?.["groups"]) {
        return false;
      }
      return state.userprofile["groups"].includes("user") &&
        !state.userprofile["groups"].includes("unconfirmed")
        ? true
        : false;
    },
    isUserInGroup: (state) => (group) => {
      if (!state.userprofile?.["groups"]) {
        return false;
      }
      return state.userprofile["groups"].includes(group) ? true : false;
    },
    isThisTheUser: (state) => (userSlug) => {
      return state.userprofile["slug"] === userSlug ? true : false;
    },
    userIsOnline: (state) => state.userIsOnline,
    authStatus: (state) => state.status,
    authErrors: (state) => state.errors,
    authErrorsLogin: (state) => state.errors["login"],
    authErrorsRegister: (state) => state.errors["register"],
    authErrorsEmailConfirmation: (state) => state.errors["emailconfirmation"],
    authHeader: (state) => {
      return {
        headers: {
          "content-type": "application/json",
          Authorization: "Bearer " + state.accessToken,
        },
      };
    },
    redirectAfterAuth: (state) => state.redirectAfterAuth,
    isLoggingOut: (state) => state.isLoggingOut,
  },
  mutations: {
    UPDATE_AUTH_PAGE_TAB(state, payload) {
      state.authPageTab = payload;
    },
    UPDATE_USER_LANGUAGE(state, payload) {
      state.userprofile.userlanguage = payload;
    },
    emailToConfirm(state, payload) {
      state.emailToConfirm = payload;
    },
    registerGeneratedSlug(state, payload) {
      if (payload) {
        state.generatedSlug = payload
          .toLowerCase()
          .replace(/-/g, " ")
          .replace(/[\p{P}$+<=>^`|~]/gu, "")
          .replace(/\s{1,}/g, "-");
      }
    },
    slugUnique(state, payload) {
      state.newSlugIsUnique = payload;
    },
    tokenRefreshState(state, payload) {
      state.tokenRefreshState = payload;
    },
    lastTokenRefreshAt(state) {
      let d = new Date();
      state.lastTokenRefreshAt = d;
      channel.postMessage({ lastTokenRefreshAt: d.getTime() });
      window.setCookie("lastTokenRefreshAt", d.getTime(), 999, true); // 999 days
    },
    initalTokenValidation(state, payload) {
      state.initalTokenValidation = payload;
    },
    auth_request(state) {
      state.status = "loading";
    },
    guest_success(state, payload) {
      state.status = "guest";
      state.accessToken = payload.accessToken;
      state.userprofile = payload.userprofile;
      state.errors = {
        login: {},
        register: {},
      };
    },
    auth_success(state, payload) {
      state.status = "success";
      state.accessToken = payload.accessToken;
      state.accessTimeOut = payload.accessTimeOut;
      state.refreshToken = payload.refreshToken;
      state.userprofile = payload.userprofile;
      state.errors = {
        login: {},
        register: {},
      };
    },
    SET_USER_ONLINE_STATUS(state, payload) {
      state.userIsOnline = payload;
    },
    auth_error(state, payload) {
      state.status = "error";
      if (payload.errorWith) {
        Vue.set(state.errors, payload.errorWith, payload.err);
      }
    },
    auth_change_reset_token_validation_state(state, payload) {
      state.resetTokenValidation = payload;
    },
    auth_change_password_state(state, payload) {
      state.resetPasswordState = payload;
    },
    auth_change_reset_token_validation_at(state, payload) {
      state.resetTokenRequestedAt = payload;
    },
    auth_resend_email_confirmation_at(state, payload) {
      state.resendConfirmationRequestedAt = payload;
    },

    logout(state) {
      state.userprofile = {
        userID: "",
        lname: "",
        fname: "",
        slug: "",
        userlanguage: "",
      };
      state.status = "";
      state.accessToken = "";
      state.refreshToken = "";
    },

    set_PKCE_code_challenge(state, payload) {
      state.PKCECodeChallenge = payload;
    },
    setRedirectAfterAuth(state, payload) {
      state.redirectAfterAuth = payload;
    },
    setIsLoggingOut(state, payload) {
      state.isLoggingOut = payload;
    },
  },
  actions: {
    UPDATE_AUTH_PAGE_TAB({ commit }, payload) {
      commit("UPDATE_AUTH_PAGE_TAB", payload);
    },
    UPDATE_USER_LANGUAGE({ commit }, payload) {
      commit("UPDATE_USER_LANGUAGE", payload);
    },
    UPDATE_USER_ONLINE_STATUS({ commit }, payload) {
      commit("SET_USER_ONLINE_STATUS", payload);
    },
    async fetchAuthenticatedUser({ state, commit }) {
      if (
        state.userprofile &&
        state.userprofile.userID &&
        state.accessToken &&
        state.refreshToken
      ) {
        return true;
      } else {
        try {
          let accessToken = window.getCookie("accessToken");
          let accessTimeOut = window.getCookie("accessTimeOut");
          let refreshToken = window.getCookie("refreshToken");
          let userprofile = JSON.parse(window.getCookie("userprofile")) || {};

          commit("auth_success", {
            accessToken: accessToken,
            accessTimeOut: accessTimeOut,
            refreshToken: refreshToken,
            userprofile: userprofile,
          });

          if (
            userprofile.userID &&
            userprofile.slug &&
            userprofile.fname &&
            userprofile.lname &&
            userprofile.userlanguage &&
            userprofile.votevalue &&
            !userprofile.guest &&
            accessToken &&
            refreshToken
          ) {
            return true;
          } else {
            return false;
          }
        } catch (err) {
          return false;
        }
      }
    },
    async logout({ commit, dispatch, getters }, payload = null) {
      commit("setIsLoggingOut", true);
      const lang = localStorage.locale;
      router.push({ path: "/" + lang + "/auth" });
      commit("logout");

      if (payload === true) {
        channel.postMessage({ logout: true });
      }

      await navigator.locks.request("logout", async () => {
        if (window.getCookie("accessToken")) {
          var url =
            process.env.VUE_APP_AUTHAPIBASEURL +
            "token/destroy?client_id=" +
            process.env.VUE_APP_AUTHAPICLIENTID;

          await axios
            .delete(url, getters["authHeader"])
            .then(async () => {})
            .catch((err) => {
              console.log(err);
            });
        }
        deleteAuthCookie();
      });
      dispatch("clearUserData");
      commit("setIsLoggingOut", false);

    },

    async login({ commit, dispatch, state }, user) {
      if (state.userIsOnline === false) {
        return false;
      }
      var url =
        process.env.VUE_APP_AUTHAPIBASEURL +
        "auth/login?client_id=" +
        process.env.VUE_APP_AUTHAPICLIENTID;
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: user,
          method: "POST",
        })
          .then(async (resp) => {
            await dispatch("setUser", resp.data);
            commit("lastTokenRefreshAt");
            // dispatch('performOneTimeActions', langtograb);
            // dispatch('performOneTimeActions');
            resolve(resp);
          })
          .catch((err) => {
            commit("auth_error", { errorWith: "login", err });
            commit(err);
            localStorage.removeItem("token");
            reject(err);
          });
      });
    },
    async getGuestToken({ commit, dispatch, state }) {
      if (state.userIsOnline === false) {
        return false;
      }
      var url = process.env.VUE_APP_AUTHAPIBASEURL + "token/guest";
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: {
            client_id: process.env.VUE_APP_AUTHAPICLIENTID,
            client_secret: "wts2",
            grant_type: "client_credentials",
          },
          method: "POST",
        })
          .then(async (resp) => {
            await dispatch("setUser", resp.data);

            resolve(resp);
          })
          .catch((err) => {
            commit("auth_error", { errorWith: "login", err });
            commit(err);
            localStorage.removeItem("token");
            reject(err);
          });
      });
    },
    setInitalTokenValidation({ commit }, payload) {
      commit("initalTokenValidation", payload);
    },
    async refreshAccessToken({ commit, dispatch, state, getters }) {
      let lastTokenRefreshAt = window.getCookie("lastTokenRefreshAt");
      let diff = new Date().getTime() - lastTokenRefreshAt;
      // If the difference isn't even half of what we want then it's no good
      if (diff < 60000) {
        return false;
      }

      if (state.userIsOnline) {
        commit("tokenRefreshState", "refreshing");
        var url =
          `token/refresh?grant_type=refresh_token&refresh_token=${state.refreshToken}&client_id=` +
          process.env.VUE_APP_AUTHAPICLIENTID;
        // let header = getters["authHeader"];
        // header["timeout"] = 10000;
        const instance = axios.create({
          baseURL: process.env.VUE_APP_AUTHAPIBASEURL,
          timeout: 10000,
        });
        return new Promise((resolve, reject) => {
          instance
            // ({
            //     method: 'post',
            //     url: url,
            //     timeout: 10000, // only wait for 10s
            //     headers: getters["authHeader"]
            // })
            .post(url, {}, getters["authHeader"])
            .then(async (resp) => {
              commit("tokenRefreshState", "complete");
              commit("lastTokenRefreshAt");
              await dispatch("setUser", resp.data);
              commit("initalTokenValidation", true);
              resolve(resp);
            })
            .catch((err) => {
              commit("tokenRefreshState", "error");
              console.log("Tried to refresh the token but failed");
              console.log(err);
              let whatNext = "nothing";

              // Was it because the token was invalid?
              if (err.response.status === 401) {
                console.log("Token was invalid");
                whatNext = "logout";
                // Was it because the token has already expired?
              } else if (
                err.response.status === 500 &&
                err.response.data.message ==
                  "You must be authorised to perform this action"
              ) {
                console.log("Token has already expired");
                console.log(err.response.data);
                whatNext = "logout";
              }
              // Was it because the network was down?
              else if (err.code === "ECONNABORTED") {
                console.log("Network was down");
                whatNext = "retry";
              }
              // Was it because the server was down?
              else if (err.response.status === 500) {
                console.log("Server was down");
                console.log(err.response.data);
                whatNext = "retry";
              }
              console.log("What next: " + whatNext);

              if (whatNext === "logout") {
                console.log("Logging out");
                dispatch("logout");
              } else if (whatNext === "retry") {
                console.log("Retry in 60 seconds");
                setTimeout(() => {
                  console.log("Retrying now");
                  dispatch("refreshAccessToken");
                }, 60000);
              }

              commit("auth_error", { errorWith: "refresh", err });
              reject(err);
            });
        });
      } else {
        commit("tokenRefreshState", "incomplete");
      }
    },
    async setAuthFromChannel({ commit, dispatch }, payload) {
      commit("tokenRefreshState", "complete");
      commit("lastTokenRefreshAt");
      await dispatch("setUser", payload);
    },
    async setUser({ state, commit }, responseDataPayload) {
      const accessToken = responseDataPayload.tokenData.accessToken;
      const userprofile = responseDataPayload.userData;

      if (!userprofile.guest) {
        const accessTimeOut = responseDataPayload.tokenData.accessTimeOut;
        const refreshToken = responseDataPayload.tokenData.refreshToken;

        if (state.accessToken != accessToken) {
          channel.postMessage(responseDataPayload);
        }

        await setAuthCookie(
          accessToken,
          accessTimeOut,
          refreshToken,
          userprofile
        );
        commit("auth_success", {
          accessToken: accessToken,
          accessTimeOut: accessTimeOut,
          refreshToken: refreshToken,
          userprofile: userprofile,
        });
      } else {
        alert("You are a guest");
        commit("guest_success", {
          accessToken: accessToken,
          userprofile: userprofile,
        });
      }
    },
    async confirmemail({ dispatch, commit }) {
      const urlParams = new URLSearchParams(window.location.search);
      const email = urlParams.get("email");
      const token = urlParams.get("token");

      var url = `${process.env.VUE_APP_AUTHAPIBASEURL}auth/emailconfirmation?client_id=${process.env.VUE_APP_AUTHAPICLIENTID}`;
      var postdata = { email: email, token: token };
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: postdata,
          method: "POST",
        })
          .then(async (resp) => {
            await dispatch("setUser", resp.data.newcredentials);
            router.push({
              path:
                "/" +
                resp.data.newcredentials.userData.userlanguage +
                "/welcome",
            });
            resolve(resp);
          })
          .catch((err) => {
            commit("auth_error", { errorWith: "emailconfirmation", err });
            reject(err);
          });
      });
    },
    // syncwithwts1({ commit }, token) {
    //   var url = process.env.VUE_APP_AUTHAPIBASEURL + "auth/usersync";
    //   return new Promise((resolve, reject) => {
    //     commit("auth_request");
    //     axios({
    //       url: url,
    //       data: { wts2sync_token: token },
    //       method: "POST",
    //     })
    //       .then((resp) => {
    //         const token = resp.data.token;
    //         const userprofile = resp.data.userprofile;
    //         setAuthCookie(token, userprofile);

    //         commit("auth_success", { token: token, userprofile: userprofile });
    //         router.push({ path: "/" });

    //         resolve(resp);
    //       })
    //       .catch((err) => {
    //         if (err.response?.data?.message == "This user already exists") {
    //           commit("auth_error", {
    //             errorWith: "register",
    //             err: err.response.data.message,
    //           });
    //         } else {
    //           commit("auth_error", { errorWith: "register", err });
    //         }
    //         console.log(err.response);
    //         localStorage.removeItem("token");

    //         reject(err);
    //       });
    //   });
    // },
    register({ commit, dispatch, state }, user) {
      if (state.userIsOnline === false) {
        return false;
      }
      var url =
        process.env.VUE_APP_AUTHAPIBASEURL +
        "auth/register?client_id=" +
        process.env.VUE_APP_AUTHAPICLIENTID;
      commit("auth_error", {
        errorWith: "register",
        err: null,
      });
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios({
          url: url,
          data: user,
          method: "POST",
        })
          .then(async (resp) => {
            // localStorage.setItem("token", token);
            await dispatch("setUser", resp.data);
            dispatch("CaptchaStore/CAPTCHA_RESET", null, { root: true });
            commit("emailToConfirm", user.email);
            router.push({
              name: "Confirm page",
              params: { LANG: state.userprofile["userlanguage"] },
            });
            resolve(resp);
          })
          .catch((err) => {
            dispatch("CaptchaStore/CAPTCHA_RESET", null, { root: true });

            if (err.response?.data?.message == "This user already exists") {
              commit("auth_error", {
                errorWith: "register",
                err: err.response.data.message,
              });
            } else {
              commit("auth_error", { errorWith: "register", err });
            }

            reject(err);
          });
      });
    },

    resendconfirmation({ commit, state, rootGetters }) {
      var url =
        process.env.VUE_APP_AUTHAPIBASEURL + "auth/resendemailconfirmation";
      // return false;

      commit("auth_resend_email_confirmation_at", new Date());
      return new Promise((resolve, reject) => {
        axios
          .post(url, state.userprofile, rootGetters["UserStore/authHeader"])
          .then((resp) => {
            resolve(resp);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },

    sendpasswordreset({ commit }, email) {
      var url = process.env.VUE_APP_AUTHAPIBASEURL + "auth/sendpasswordreset?client_id=" +
      process.env.VUE_APP_AUTHAPICLIENTID;

      commit("auth_change_reset_token_validation_at", new Date());
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: { email: email },
          method: "POST",
        })
          .then((resp) => {
            resolve(resp);
          })
          .catch((err) => {
            reject(err);
          });
      });
    },
    checkresettoken({ commit }) {
      const urlParams = new URLSearchParams(window.location.search);
      const email = urlParams.get("email");
      const token = urlParams.get("token"); 
      var url = process.env.VUE_APP_AUTHAPIBASEURL + "auth/checkresettoken?client_id=" +
      process.env.VUE_APP_AUTHAPICLIENTID;
      commit("auth_change_reset_token_validation_state", "checking");
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: { email: email, token: token },
          method: "POST",
        })
          .then((resp) => {
            commit("auth_change_reset_token_validation_state", "validated");
            resolve(resp);
          })
          .catch((err) => {
            commit("auth_change_reset_token_validation_state", "error");
            reject(err);
          });
      });
    },
    async checkauthtoken({ state, getters, dispatch }) {
      if (state.userIsOnline) {
        var url = process.env.VUE_APP_AUTHAPIBASEURL + "auth/checktoken";
        await axios
          .post(url, { token: state.accessToken }, getters["authHeader"])

          .then(function () {
            // Token is valid
          })
          .catch(() => {
            dispatch("refreshToken");
          });
      }
    },

    changepassword({ commit }, payload) {
      const urlParams = new URLSearchParams(window.location.search);
      const email = urlParams.get("email");
      const token = urlParams.get("token");
      var url = process.env.VUE_APP_AUTHAPIBASEURL + "auth/changepassword?client_id=" +
      process.env.VUE_APP_AUTHAPICLIENTID;
      commit("auth_change_password_state", "checking");
      return new Promise((resolve, reject) => {
        axios({
          url: url,
          data: { email: email, token: token, password: payload },
          method: "POST",
        })
          .then((resp) => {
            commit("auth_change_password_state", "changed");
            resolve(resp);
          })
          .catch((err) => {
            commit("auth_change_password_state", "error");
            reject(err);
          });
      });
    },
    async checkSlug({ commit }, payloadSlug) {
      commit("registerGeneratedSlug", payloadSlug);
      if (!(await fetchSlugExist(payloadSlug))) {
        commit("slugUnique", true);
      } else {
        commit("slugUnique", false);
      }
    },
    async findSlug({ commit, dispatch }, payload) {
      const newSlug =
        payload.user.fname.toLowerCase() +
        "-" +
        payload.user.lname.toLowerCase() +
        (payload.attempt ? "-" + payload.attempt : "");
      commit("registerGeneratedSlug", newSlug);
      let exists = await fetchSlugExist(newSlug);
      commit("slugUnique", exists ? false : true);
      if (exists === true) {
        if (!payload?.attempt) payload.attempt = 1;

        setTimeout(() => {
          dispatch("findSlug", {
            attempt: payload.attempt + Math.round(10 + Math.random() * 100),
            user: payload.user,
          });
        }, 200);
      }
    },
    performOneTimeActions({ dispatch }) {
      dispatch("PreferenceStore/FETCH_PREFERENCES", null, { root: true });
      dispatch("FollowStore/FETCH_WHO_I_FOLLOW", null, { root: true });
      dispatch("TrustStore/FETCH_WHO_I_TRUST", null, { root: true });
      dispatch("VoteStore/FETCH_VOTES", null, { root: true });
      dispatch("SocketStore/SOCKET_CONNECT", null, { root: true });
      dispatch("InboxStore/FETCH_NOTIFICATIONS", null, { root: true });
      dispatch("BlockUser/FETCH_BLOCKED_USERS", null, { root: true });
    },
    clearUserData({ dispatch }) {
      dispatch("FollowStore/CLEAR_WHO_I_FOLLOW", null, { root: true });
      dispatch("TrustStore/CLEAR_WHO_I_TRUST", null, { root: true });
      dispatch("BlockUser/CLEAR_WHO_I_BLOCKED", null, { root: true });

      dispatch("PreferenceStore/CLEAR_PREFERENCES", null, { root: true });
      dispatch("VoteStore/CLEAR_VOTES", null, { root: true });
      dispatch("SocketStore/SOCKET_DESTROY", null, { root: true });
    },
    makePKCE({ commit }) {
      let nonce = cryptGen.randomBytes(128).toString();
      setPKCECookie(nonce);

      const utf8 = new TextEncoder().encode(nonce);
      crypto.subtle.digest("SHA-256", utf8).then((hashBuffer) => {
        const hashArray = Array.from(new Uint8Array(hashBuffer));
        const hashHex = hashArray
          .map((bytes) => bytes.toString(16).padStart(2, "0"))
          .join("");

        let challenge = window.btoa(hashHex);
        commit("set_PKCE_code_challenge", challenge);
      });
    },
    checkIfMustLogin({ dispatch, state }, error) {
      if (error.response) {
        if ([403].includes(error.response.status) && state.accessToken) {
          dispatch("logout");
        }
        if ([500].includes(error.response.status) && state.accessToken) {
          dispatch("logout");
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log("No response or request in error");
      }
    },
    changeRedirectAfterAuth({ commit }, payload) {
      commit("setRedirectAfterAuth", payload);
    },
  },
}; //0.0034722222222222, // 1 day as 5 mins
const setPKCECookie = async (nonce) => {
  window.setCookie("pkce_nonce", nonce, 365, true);
};
const setAuthCookie = async (
  accessToken,
  accessTimeOut,
  refreshToken,
  userprofile
) => {
  if (accessToken) window.setCookie("accessToken", accessToken, 7, true); // 1 week
  if (accessTimeOut) window.setCookie("accessTimeOut", accessTimeOut, 7, true); // 1 week
  if (refreshToken) window.setCookie("refreshToken", refreshToken, 7, true); // 1 week
  if (userprofile)
    window.setCookie("userprofile", JSON.stringify(userprofile), 999, true); // 999 days
};
const deleteAuthCookie = async () => {
  window.eraseCookie("accessToken", true);
  window.eraseCookie("accessTimeOut", true);
  window.eraseCookie("refreshToken", true);
  window.eraseCookie("userprofile", true);
};
const fetchSlugExist = async (newSlug) => {
  if (newSlug.length < 3) {
    return true;
  }
  var url =
    process.env.VUE_APP_APIBASEURL +
    "userslugcheck/" +
    newSlug +
    "?client_id=" +
    process.env.VUE_APP_AUTHAPICLIENTID;
  var exists = false;
  await axios
    .get(url)
    .then(function (response) {
      // If it exists then we try again with an added number
      exists = response.data.exists;
    })
    .catch(function (error) {
      console.log(error);
    });
  return exists;
};
