import axios from "axios";


export default {
  namespaced: true,
  state: {
    VoteMemory: {},
    
  },
  getters: {    
    GET_MEMORY: (state) => {
      return state.VoteMemory;
    },
    GET_VOTE_STATUS: (state) => (payload_pk) => {
      if (state.VoteMemory[payload_pk]) {
        return state.VoteMemory[payload_pk]
      } else {
        return {}
      }
    },
  },
  mutations: {
    CLEAR_VOTES(state) {
      state.VoteMemory = {};
    },
    CHANGE_VOTE_MEMORY(state, payload) {
      state.VoteMemory[payload.sk] = {
        vote: payload.vote,        
        votevalue: payload?.votevalue ? payload.votevalue : 0,
      }
    
    },
    LOAD_VOTE_MEMORY(state, payload) {
      let i;
      for (i in payload) {
        state.VoteMemory[payload[i]['parent']['sk']] = {
          vote: payload[i]['vote'],
          votevalue: payload[i]['votevalue'],
        }  
      }
    },
    REMOVE_VOTE(state,payload){
      if (payload.sk in state.VoteMemory){
        delete state.VoteMemory[payload.sk]; 
      }
      else{
        console.log("Vote is already deleted")
      }
    }
  },
  actions: {
    CLEAR_VOTES({ commit }, payload) {
      commit("CLEAR_VOTES", payload);
    },
    REMOVE_VOTE({commit}, payload){
      commit("REMOVE_VOTE", payload)
    },
    ARCHIVE_VOTE({dispatch, rootGetters}, payload) {
      var url = process.env.VUE_APP_MODERATIONBASEURL + "archiveuservote";
      var putdata = {
        userslug: payload.userslug,
        slug: payload.parent.slug
      }
      axios
        .put(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if(response.status === 200){
            console.log("Yay it worked")
            let toupdate = payload.parent
            dispatch("REMOVE_VOTE", toupdate)
          } else {
            console.log("something went wrong");
          }
        })
    },
    CAST_VOTE({ commit, rootGetters }, payload) {
      var url = process.env.VUE_APP_APIBASEURL + "votecast";
      var putdata = {
        parent: payload.parent,
        vote: payload.vote
      };
      commit("CHANGE_VOTE_MEMORY", {
        sk: payload.parent.sk,
        vote: payload.vote,
        votevalue: rootGetters["UserStore/userVoteValue"],
      });
      axios
        .post(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if(response.status === 200){
            let responsedata = JSON.parse(response.config.data)
            commit("CHANGE_VOTE_MEMORY", {
              sk: responsedata.parent.sk,
              vote: responsedata.vote,
              votevalue: rootGetters["UserStore/userVoteValue"],
            });
          } else {
            console.log("No vote recorded");
          }
        })
        .catch(function (error) {
          console.log(error)
        });
    },
    FETCH_VOTES({ commit, rootGetters }) {
      var url;
      url =
        process.env.VUE_APP_APIBASEURL +
        "votesgetmine"
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response)
          commit("LOAD_VOTE_MEMORY", response.data);
        })
        .catch(function (error) {
          console.log("Follow response:");
          console.log(error);
          // dispatch('UserStore/checkIfMustLogin', error, {root: true});

        });
      
    },
  }
};
