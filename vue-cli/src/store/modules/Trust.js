import axios from "axios";
import Vue from "vue";

export default {
  namespaced: true,
  state: {
    trustMemory: {},
    trustOverview: [],
    fetching: false
  },
  getters: {
    GET_TRUST_MEMORY: (state) => {
      return state.trustMemory;
    },
    GET_TRUST_OVERVIEW: (state) => {
      return state.trustOverview;
    },
    GET_TRUST_FETCHING: (state) => {
      return state.fetching;
    },

    GET_TRUST_STATUS: (state, getters) => (userSlug) => {
      if (userSlug) {
        let slug = getters["TRUST_SLUG"]({ slug: userSlug });
        if (state.trustMemory[slug]) {
          return state.trustMemory[slug];
        }
      }       
      return {}      
      
    },  
    TRUST_SLUG: (state, getters, rootState) => (payload) => {
      return `trust-${payload.slug.trim()}_${
        rootState.UserStore.userprofile.slug
      }`;
    },
  },
  mutations: {
    CLEAR_TRUST_MEMORY(state) {
      state.trustMemory = {};
    },
    CHANGE_TRUST_MEMORY(state, payload) {
      Vue.set(state.trustMemory, payload.slug, {
        slug: payload.slug,
        key: payload.key,
        trustLevel: payload.trustLevel,
        trustSlug: payload.trustSlug,
      });
    },
    CHANGE_TRUST_OVERVIEW(state, payload) {
      console.log(`setting trustOverview with a ` + typeof (payload));
      state.trustOverview = payload;
    },
    CHANGE_TRUST_FETCHING(state, payload) {

      state.fetching = payload;
    },
    REMOVE_FROM_TRUST_MEMORY(state, payload) {

      Vue.delete(state.trustMemory, payload);
    },
  },
  actions: {
    CLEAR_WHO_I_TRUST({ commit }) {
      commit("CLEAR_TRUST_MEMORY");
    },
    CLEAR_TRUST_OVERVIEW({ commit }) {
      
      commit("CHANGE_TRUST_OVERVIEW", []);   

    },
    FETCH_USER_TRUST({ commit, rootGetters }, payload) {
      var url;
      var direction = payload.direction == 'rated-by' ? "userinit" :  "userhas" ;
        
      url = `${process.env.VUE_APP_APIBASEURL}reltrust/${direction}/${payload.userSlug}`;
      commit('CHANGE_TRUST_FETCHING', true);
      axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit('CHANGE_TRUST_FETCHING', false);
          if (response.data.Items) {
            commit("CHANGE_TRUST_OVERVIEW", response.data.Items);
          }
          
        })
        .catch(function (error) {
          commit('CHANGE_TRUST_FETCHING', false);
          console.log("Cannot fetch user trust [rated]:");
          console.log(error);
        });
    },
    FETCH_WHO_I_TRUST({ commit, getters, rootGetters }) {
      var url;
      url =
        process.env.VUE_APP_APIBASEURL +
        "reltrust/userinit/" +
        rootGetters["UserStore/userProfile"].slug;

      axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          let i;
          for (i in response.data.Items) {
            commit("CHANGE_TRUST_MEMORY", {
              key: {
                pk: response.data.Items[i].pk,
                sk: response.data.Items[i].sk,
              },
              slug: getters["TRUST_SLUG"]({
                slug: response.data.Items[i].parentSlug,
              }),
              trustLevel: response.data.Items[i].trustLevel,
              trustSlug: response.data.Items[i].parentSlug,
            });
          }
          // commit("PUSH_TRUST_MEMORY", response.data.Items);
        })
        .catch(function (error) {
          console.log("Follow response:");
          console.log(error);
        });
    },
    async FETCH_TRUST_STATUS({ commit, getters, rootGetters }, payload) {
      if (payload) {
        var url;
        url =
          process.env.VUE_APP_APIBASEURL +
          "reltrust/get/userprofile/" +
          payload.slug.trim();

        await axios
          .get(encodeURI(url), rootGetters["UserStore/authHeader"])
          .then(function (response) {
            commit("CHANGE_TRUST_MEMORY", {
              key: { pk: response.data.pk, sk: response.data.sk },
              slug: getters["TRUST_SLUG"](payload),
              trustSlug: payload.slug,
              trustLevel: response.data.trustLevel,
            });
          })
          .catch(function (error) {
            console.log("Trust response error:");
            console.log(error);
          });
      }
    },
    CREATE_TRUST_RELATIONSHIP({ commit, rootGetters, getters }, payload) {
      var url = process.env.VUE_APP_APIBASEURL + "reltrust";
      var putdata = {
        parent: { sk: payload.parentSK },
        parentSlug: payload.slug,
        trustLevel: payload.trustLevel,
      };
      let slug = getters["TRUST_SLUG"]({ slug: payload.slug });
      commit("CHANGE_TRUST_MEMORY", {
        slug: slug,
        key: { pk: payload.pk, sk: payload.sk },
        trustSlug: payload.slug,
        trustLevel: payload.trustLevel,
      });
      axios
        .post(url, putdata, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          if (typeof response.data.pk == "undefined") {
            console.log("No id");
          }
          commit("CHANGE_TRUST_MEMORY", {
            slug: slug,
            key: { pk: response.data.pk, sk: response.data.sk },
            trustSlug: payload.slug,
            trustLevel: payload.trustLevel,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    SET_MY_TRUST_LEVEL({ commit, getters }, payload) {
      commit("CHANGE_TRUST_MEMORY", {
        key: payload.key,
        slug: getters["TRUST_SLUG"](payload),
        trustSlug: payload.slug,
        trustLevel: payload.trustLevel,
      });
    },
    CLEAR_TRUST_LEVEL({ commit }, payload) {
      commit("REMOVE_FROM_TRUST_MEMORY", payload);
    },
    CHANGE_TRUST_RELATIONSHIP(
      { commit, rootGetters, getters, state },
      payload
    ) {
      var url = process.env.VUE_APP_APIBASEURL + "reltrust";
      let slug = getters["TRUST_SLUG"]({ slug: payload.slug });

      var putdata = {
        key: state.trustMemory[slug].key,
        trustLevel: payload.trustLevel,
      };
      if (typeof payload.trustLevel !== "undefined") {

      
        commit("CHANGE_TRUST_MEMORY", {
          slug: slug,
          key: payload.key,
          trustSlug: payload.slug,
          trustLevel: payload.trustLevel,
        });
        axios
          .put(url, putdata, rootGetters["UserStore/authHeader"])
          .then(function (response) {
            if (typeof response.data.message == "undefined") {
              console.log("No message");
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    },
  },
};
