Line: Text
——————————
   4: This page was probably assimilated by some pesky Borg and is no longer 'here'
  10: All Subwikis
  16: Close
  18: Whoops! Nothing to fetch comments on!
  22: Continue reading
  30: created new vote
  35: Edit attached link
  36: Edit Copy
  37: Edit this post
  52: Loading User
  57: Manage API Access
  65: NO USER
  69: One off
  70: Oops! Looks like you've entered the wrong login
  71: Pages
  83: Posts
  87: Random Box
  88: You have read all the posts here!
 102: Settings
 105: Subwikis
 109: There is a problem with your account
 110: Trust
 125: You have been disconnected from the internet
 126: You need to be online to use Wikitribune Social
 127: Your Client ID's
 128: Your connection has been restored
 146: Admin Stuff
 147: You are now editing the about page
 148: Get User Groups
 151: Archive
 152: Are you sure you would like to archive this post?
 153: Archive Post
 154: Cancel
 158: Checking details...
 171: A Beginner's Guide to WT.Social
 173: Check here for the latest announcements from the official WTS team.
 176: The Basics
 178: What makes WTS different from Facebook?
 182: What makes a 'quality post'?
 186: Editing A Post
 187: Yes, you can edit collaborative posts! By clicking on the edit icon, you can collaborate with other users on fact checks, fix headlines, or help fight against spam, misinformation, and hate speech. If you want to try it out, give this post an update.
 188: The Edit Button
 189: What's a collaborative post?
 192: Hiding Comments
 193: UNDER CONSTRUCTION
 197: Using The Website
 199: About Your Feed
 203: About Subwikis
 207: About Recent Changes
 212: How To
 214: Change Your Settings
 215: You can update your language, profile, and notification settings in the 'My Account' screen at any time. If you would like to help translate the site into your language, let us know in the WTS Features subwiki.
 218: Talking To Another User
 219: WTS does not have private messaging, but you can leave someone a message by clicking on their username and creating a new post on their feed.
 222: Invite Your Friends, Family, and Fans
 223: THIS ISN'T STARTED YET
 226: Share a Post
 227: THIS ISN'T STARTED YET
 230: Keeping Track of Conversations
 234: Using Hashtags
 235: THIS ISN'T STARTED AND WE NEED A COMMUNITY DISCUSSION ABOUT IT
 239: More Info
 241: The Difference Between a Collaborative and Individual Post
 245: Who Runs This Place?
 249: A Comprehensive List of Subwikis About WT.Social
 258: TERMS OF SERVICE
 259: Self Promotion Policy
 260: NSFW Policy
 261: Names and Pseudonyms
 266: Placeholder contact us paragraph
 310: How Can I Bring My Followers To WT.Social?
 315: Users like you. We have not taken VC money, nor do we allow advertisers on our platform. We rely on the generosity of our community to keep the lights on. If you would like to help, please consider making a contribution <a href='https://contribute.wt.social/'>here</a>.
 318: Can I Promote My Blog/Podcast/Business Here?
 319: You sure can! So long as you follow our Self-Promotion Guidelines as set out <a href='https://wt.social/self-promotion'>here</a>.
 322: Why Is It Called Wt.Social?
 326: Are You Open To Being On The Blockchain?
 331: You're not following anybody
 332: People You Follow
 335: My Subwikis
 336: You are not currently following any subwikis.
 337: Browse Subwikis
 345: Login now
 347: There was a problem validating the reset token.
 348: Please be sure to follow the full link from your email. If you continue to have issues please
 350: resetting your password again
 354: Choose your plan
 363: Provider
 365: If you would like to make an account
 375: Fetching payment schedules
 376: There was an error fetching your payment schedules
 377: Term
 378: Status
 379: Created
 380: Provider
 421: by
 424: Roadmap of Features
 426: Complete
 427: Testing
 428: In Progress
 429: Ongoing
 430: Paused
 431: Awaiting
 435: Trust Rating
 436: The first step to Jimmy's new vision is allowing users to rate each other.
 439: Mass archiving of a user's stuff
 440: Typically used as part of blocking/account deletion
 443: Let everyone see who trusts someone and how much
 444: So we can all discuss how it's being used
 448: Allow users to express displeasure with someone
 451: Let everyone see who is trusted by a user
 452: So we can all discuss how it's being user
 455: Let everyone see who is highly ranked on the site
 456: Simple table or tables to show the top users
 459: Trust Mechanism: Cause and effect
 460: Implement first extremely basic algorithm to sort discover tab by time AND quality
 463: People suggestions in Onboarding
 467: Nuke a post and all comments entirely
 471: See who people are following and who follows them
 472: So we can look into it
 475: Bell Comment notifications
 476: Bell notify me when someone comments on my post or my comment or edits those
 479: Declutter notification
 480: Remove bell notificaiton when someone I follow posts
 483: Have email notifications
 484: Email notifications
 487: 24 and 72 hour new user emails
 488: The standard sort of emails but tells them what they need to do for promotion
 491: Subwiki Moderation
 492: Deleting and merging subwikis are tools we will need
 495: Moderation Center
 496: A place for people to discuss and debate any issues with the content
 499: Beginners Guide
 500: To help people get stuck in we will need this helpful guide
 503: Open source WTS2
 504: This will be an exciting day but there are some things we need to do first.
 507: 'Follow me' links
 508: A feature we have on WTS1 which will help spread the word
 511: Ability to follow a post
 512: One of the most requested features
 515: Newsletter Preferences
 516: Before we can transition to WTS2 this will need doing
 519: Pinned posts
 520: A staple feature from WTS1 that is still missing from WTS2
 523: Re-posting
 524: A staple feature from WTS1 that is still missing from WTS2
 527: User to user blocking
 528: A staple feature from WTS1 that is still missing from WTS2
 531: Ability to disable collaborative editing
 532: A staple feature from WTS1 that is still missing from WTS2
 535: Launch MVP
 536: At this point we can switch over to WTS2 permanently
 539: Regional personalisations
 540: ie- Showing times in your timezone and local format
 543: Post/comment/recent changes translation
 544: We have been wanting to view posts in various languages for a long time now
 547: Searching
 548: The search algorithm for WTS1 is awful and we want to make sure its replacement is actually usable
 551: Video uploads
 552: Posting videos to social media websites is commonplace and should be something we support
 555: OAuth Authorization Code Flow with PKCE
 556: With this we will be able to start work on a mobile app or allow others to have a go.
 562: Dear Supporters
 570: But to finish, I need your support.  I have to pay for the development work to finish the new version of the site, and I need to do some PR and marketing to get the word out.  I need to really start pushing this forward as a relaunch. If each of you can afford to give even a little, I think we have a real shot at making something revolutionary and new.  We won't know unless we keep trying.
 575: Due
 576: Paid
 579: Invoice
 580: Pending
 581: Closed
 589: Fetching invoices
 590: Oops - There was an error fetching your invoice details
 596: There was an error displaying the payment form:
 601: Describe this subwiki
 608: A thoughtful and collaborative social media platform
 609: A site driven by quality content, not outrage
 610: Thoughtful design, and a step away from dopamine-driven social addiction
 612: If you would like to support us at a higher level, you can join us as a Patron.
 613: Find out more today
 614: No thank you
 617: You don't have to pay - but here's why I am asking you to do it!
 618: It's only a matter of time before Elon Musk follows through on his threat to reinstate Donald Trump and allow as much abuse and disinformation as legally possible.
 619: If you think that's a sad future for the world, please help me by setting up a small monthly or annual donation - and by helping to spread the word.  We need to improve the software and I want to do it without pressure from investors to accept ads and algorithms aimed at addiction.
 630: Fetched in {time} seconds from {source}
 633: I don't know this user at all
 634: Hmm
 635: Maybe
 636: Okay
 637: Great!
 638: I would always trust this user!
 639: People I trust
 640: People who trust me
 641: No users have rated {user}
 642: Ratings by this user
 643: Rated by this user
 644: Set your trust level for this user
 645: {user} has not rated any users
 646: Users rated by
 647: Users who rated
 648: rated {user2} <strong>{trustLevel}%</strong>
 649: was rated <strong>{trustLevel}%</strong> by {user2}
 650: This is you
 651: You rated this user:
 654: Thanks for signing up for WikiTribune Social 2.0. Here's a few things to get you started:
 655: Skip Onboarding
 656: Subwikis to follow:
 659: Click here for an in-depth Beginner's Guide to WTS
 660: Choose your theme
 661: Finish onboarding
 662: Dive In
 671: archived a post
 680: created new vote
 682: Number of comments
 683: Number of posts
 684: Subwikis created
 685: Total Followers
 686: Total Profile Posts
 687: Profile age
 688: Total actions
