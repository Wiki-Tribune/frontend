#!/bin/env python3
import yaml
from googletrans import Translator

# instantiate a translator
tl = Translator()

data = yaml.safe_load(open("esPleaseTranslate.yaml"))
for k,v in data.items():
    print(f"{k=}\n{v=}\n")
    if isinstance(v, str):
        data[k] = tl.translate(v, dest="es").__dict__()["text"]
    if isinstance(v, dict):
        for kk,vv in v.items():
            print(f"{kk=}\n{vv=}\n")
            if isinstance(vv, str):
                data[k][kk] = tl.translate(vv, dest="es").__dict__()["text"]

yaml.dump(data, open("esTranslated.yaml", "w", encoding="utf-8"), sort_keys=True, allow_unicode=True)